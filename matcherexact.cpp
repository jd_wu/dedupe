/*
 * MatcherExact.cpp
 *
 *  Created on: 2013-5-21
 *      Author: Judy
 */

#include <stdio.h>
#include <stdlib.h>
//#include <regex>
#include<memory.h>
#include<algorithm>
#include"utils.h"
#include"matcherexact.h"
#include "deduper.h"
#include "alginterface.h"
#include"dedupelog.h"

extern "C"
{
#include"jellyfish.h"
}
//#include <regex.h>
#include <boost/regex.hpp>
using namespace utils;
using namespace std;

//const variable:
const string nameprefixes[] =
  { "Able Seaman", "Air Commodore", "Air Marshall", "Air Vice-Marshall",
      "Archbishop", "Archdeacon", "Assoc", "Associate", "Baron", "Baroness",
      "Bishop", "Br", "Brig", "Brigadier", "Brother", "Cadet", "Canon", "Capt",
      "Captain", "Cardinal", "Chap", "Chapl", "Chaplain", "Chapln",
      "Chief Petty Officer", "Cmd", "Col", "Colonel", "Commander",
      "Commissioner", "Commodore", "Corporal", "Councillor", "Count",
      "Countess", "Cpl", "Deacon", "Deaconess", "Detective",
      "Detective Sergeant", "Doctor", "Dr", "Emeritus", "Father",
      "Flight Lieutenant", "Flight Sargeant", "Flight Sergeant", "Flt Lt",
      "Flt Sgt", "Flying Officer", "Fr", "Gen", "General", "Group Captain",
      "Grp Capt", "Grp Captain", "Her Excellency", "Her Hon", "Her Honour",
      "Her Worship", "His Excellency", "His Hon", "His Honour", "His Worship",
      "Hon", "Honorable", "Inspector", "Judge", "Justice", "King", "L-Cpl",
      "Lady", "Lance Corporal", "Lcdr", "LCpl", "Leading Aircraftsman",
      "Leading Aircraftswoman", "Leading Seaman", "Lieut", "Lieut Col",
      "Lieut Colonel", "Lieut Commander", "Lieut-Col", "Lieut-Colonel",
      "Lieut-Commander", "Lieutenant", "Lord", "Lt Col", "Lt Colonel",
      "Lt Commander", "Lt Gen", "Lt-Col", "Lt-Colonel", "Lt-Commander", "M-s",
      "M/s", "Madam", "Magistrate", "Maj", "Maj-Gen", "Major", "Major General",
      "Major-General", "MajorGeneral", "Master", "Masters", "Messr", "Messrs",
      "Midshipman", "Miss", "Misses", "Mister", "Mjr", "Monsignor", "Most",
      "Mother", "Mr", "Mr/Mrs", "Mr/s", "Mrs", "Ms", "Msgr", "Mstr", "Mstrs",
      "Officer Cadet", "Pastor", "Pilot Officer", "Pope", "Pres", "President",
      "Prince", "Princess", "Private", "Prof", "Professor", "Pte", "Pvt",
      "Queen", "Rabbi", "Rev", "Rev'd", "Revd", "Reverend", "Right Honorable",
      "Rt Hon", "Sargeant", "Sen", "Senator", "Sergeant", "Sgt", "Signalman",
      "Signalwoman", "Sir", "Sister", "Sqdn Ldr", "Squadron Leader", "Sr",
      "Staff Sargeant", "Staff Sergeant", "Sub Lieut", "Sub Lieutenant",
      "Superintendent", "Supt", "The Honorable", "The Most",
      "The Right Honorable", "The Rt Hon", "The Venerable", "The Very",
      "Venerable", "Very", "W/O", "Warrant Officer", "Wing Commander",
      "Wng Cmdr" };
const string namesuffixes[] =
  { "AC", "AM", "AO", "B A", "B Sc", "B Sc(Tech)", "BA", "BSc", "BSc(Tech)",
      "C S", "CBE", "DBE", "DSO", "Esq", "Esquire", "I", "II", "III", "IV",
      "J P", "Jnr", "JP", "Jr", "KBE", "KCMG", "KM", "M B E", "M D", "M P",
      "MBE", "MD", "MHR", "MLC", "MP", "MVO", "O A M", "O B E", "OAM", "OBE",
      "OM", "Q C", "QC", "San", "SC", "Snr", "SP", "TD", "V", "VI" };
const string maleprefixes[] =
  { "Baron", "Brother", "Count", "Deacon", "Father", "Fr", "His Excellency",
      "His Hon", "His Honour", "His Worship", "Lord", "Sir", "Master",
      "Masters", "Messr", "Messrs", "Mister", "Mr", "Mstr", "Mstrs", "Prince",
      "King", "Monsignor", "Signalman" };
const string femaleprefixes[] =
  { "Baroness", "Sister", "Countess", "Deaconess", "Mother", "Her Excellency",
      "Her Hon", "Her Honour", "Her Worship", "Lady", "Madam", "Miss", "Misses",
      "Mrs", "Ms", "Princess", "Queen", "Signalwoman" };
const string companies[] =
  { "trust", "limited", "ltd", "proprietary", "pty", "trustee", "estate of" };
const string unitprefixes[] =
  { "unit", "apartment", "apt" };
const string regionstopwords[] =
  { "north", "east", "west", "south", "n", "e", "w", "s", "dc" };


const string addresspostfixes[] =
    {"alley","al","amble","amb","approach","appr",
        "arcade","arc","arterial","art","avenue","av", "ave", "av", "bay","bay","bend","bend","brae","brae","break","brk",
        "boulevard","bvd","boardwalk","bwk","bowl","bwl","bypass","byp","circle","ccl","circus","ccs","circuit",
        "cct","chase","cha","close","cl","corner","cnr","common","com","concourse","con","crescent","cr","cross",
        "cros","course","crse","crest","crst","cruiseway","cry","court/s","ct","cove","cv","dale","dale","dell","dell",
        "dene","dene","divide","div","domain","dom","drive","dr","east","east","edge","edg","entrance","ent",
        "esplanade","esp","extension","extn","flats","flts","ford","ford","freeway","fwy","gate","gate","garden/s","gdn",
        "glade/s","gla","glen","gln","gully","gly","grange","gra","green","grn","grove","gv","gateway","gwy","hill","hill",
        "hollow","hlw","heath","hth","heights","hts","hub","hub","highway","hwy","island","id","junction","jct","lane","la",
        "link","lnk","loop","loop","lower","lwr","laneway","lwy","mall","mall","mew","mew","mews","mws","nook","nook",
        "north","nth","outlook","out","path","path","parade","pd/pde","pocket","pkt","parkway","pkw","place","pl","plaza",
        "plz","promenade","prm","pass","ps","passage","psg","point","pt","pursuit","pur","pathway","pway","quadrant","qd",
        "quay","qu","reach","rch","road","rd","ridge","rdg","reserve","rest","rest","rest","retreat","ret","ride","ride","rise","rise",
        "round","rnd","row","row","rising","rsg","return","rtn","run","run","slope","slo","square","sq","street","st","south","sth",
        "strip","stp","steps","stps","subway","sub","terrace","tce","throughway","thru","tor","tor","track","trk","trail","trl","turn","turn",
        "tollway","twy","upper","upr","valley","vly","vista","vst","view/s","vw","way","way","wood","wd","west","west","walk","wk",
        "walkway","wkwy","waters","wtrs","waterway","wry","wynd","wyd"};

MatcherExact::MatcherExact(const string fields)
{

  //set the match field:

  if (fields == "")
    { //if there's no match filed, then match all
      m_Fields.insert(make_pair("all", "data"));
    }
  else
    {
      string strField, strFieldName, strFieldType, tmp;
      size_t j, k, i = 0;

      do
        {
          //parse the field line, like: nameline=name, address1=address,
          j = utils::advplain(fields, strField, i);
          utils::trim(strField);
          k = strField.find("=");
          if (k != string::npos)
            { //if there's "=", then get the fieldname and filedtype
              strFieldName = strField.substr(0, k);
              strFieldType = strField.substr(k + 1, j - 1);


              utils::trim(strFieldName);
              utils::trim(strFieldType);
            }
          else
            { //if there's no "=" then just set the filedname and set "data" as fieldtype
              strFieldName = strField;
              strFieldType = "data";
            }
          tmp = strFieldName ;
          utils::tolowerstr(tmp);
          if(tmp != "suburb")
          m_Fields.insert(make_pair(strFieldName, strFieldType)); //insert m_Fields member attribute

          //fprintf(stdout, "strFieldName: %s, strFieldType: %s\n", strFieldName.c_str(), strFieldType.c_str());
          i = j + 1;
        }
      while (j < fields.length());

    }

}
int
MatcherExact::GetFieldMap(MapNoSort &fieldmap, const vector<string> &fieldnames)
{
  //每一行以逗号分隔， 可能字段中有引号

  int index = 0;
  //char strIndex[10];
  StrMapNoSort::iterator fieldsIter = m_Fields.begin();
  MapNoSort::iterator fieldmapIter;
  vector<int> vIndex;

  for (; fieldsIter != m_Fields.end(); fieldsIter++){
    if ((index = utils::getindexofvector(fieldnames, fieldsIter->first)) >= 0){
        //vector<int> vIndex;
        // vIndex.push_back(index);
        // fieldmap.insert(make_pair(fieldsIter->second, vIndex));
        //fprintf(stderr, "after getindexofvector\n");
        fieldmapIter = fieldmap.find(fieldsIter->second);
       if (fieldmapIter != fieldmap.end()){
           fieldmapIter->second.push_back(index);
            continue;
       }
       else{
           vector<int> vIndex;
           vIndex.push_back(index);
            fieldmap.insert(make_pair(fieldsIter->second, vIndex));
       }

      }
    else{
        fprintf(stderr, "error: %s is not in list\n", (fieldsIter->first).c_str());
        string msg = "error:  " + fieldsIter->first + " is not in list" + "\r\n";
        DedupeLog::GetPtr("")->WriteLog(msg);
        return -1;
      }

  }
  //fprintf(stdout, "leave GetFieldMap\n");
  return 0;

}

string
MatcherExact::GetMatchkey(const MapNoSort &fieldmap, const vector<string> &data,
  MetaData *metadata)
{

MapNoSort::const_iterator fieldmapIter;
vector<int>::const_iterator indexIter;
string fields, tmp;


/*vector<string>::const_iterator iter = data.begin();
  for (; iter != data.end(); iter++){
      fprintf(stderr, "string in vector is %s\n", (*iter).c_str());


  }*/
if (m_Fields.find("all") != m_Fields.end())
  {
    //fprintf(stdout, "find all\n");
    for (size_t i = 0; i < data.size(); i++)
      {
        tmp = data[i];
        while(tmp.find(" ") == 0){
          utils::lTrim(tmp);
        }
      fields += tmp; //all of the data is combined into a key
      fields += " ";
    }
}
else
{
  for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end();
      fieldmapIter++)
        {
    if (fieldmapIter->first == "data") //combine all of the required data togehter as the key
      {
        //fprintf(stdout, "find data\n");
        for (indexIter = fieldmapIter->second.begin();
            indexIter != fieldmapIter->second.end(); indexIter++)
              {
            //fprintf(stdout, "second index is %d\n", (*indexIter));
            tmp = data[*indexIter];
             while(tmp.find(" ") == 0){
                 utils::lTrim(tmp);
             }
             fields += tmp; //all of the data is combined into a key
             fields += " ";

        }
    }
}

}
//fprintf(stdout, "exactkey:%s\n", fields.c_str());

utils::rTrim(fields);
//fprintf(stdout, "exactkey:%s\n", fields.c_str());
return fields;
}

//compare the keys
bool
MatcherExact::Compare(string value1, string value2, MetaData *meta1,
MetaData *meta2)
{
return (value1 == value2);
}

MatcherStrip::MatcherStrip(const string fields, const string stripchars) :
MatcherExact(fields)
{
  m_Stripchars = stripchars;
  //fprintf(stdout, "strip is %s\n", m_Stripchars.c_str() );
 //escape character
//m_Stripchars = utils::unestrcape(stripchars);
//fprintf(stdout, "m_Stripchars is %s\n", m_Stripchars.c_str());

}

string
MatcherStrip::GetMatchkey(const MapNoSort &fieldmap, const vector<string> &data,
MetaData *metadata)
{

string fields = MatcherExact::GetMatchkey(fieldmap, data, metadata);


utils::tolowerstr(fields);
//fprintf(stdout, "after tolower stripkey:%s\n", fields.c_str());
//fprintf(stdout, "m_Stripchars is %s\n", m_Stripchars.c_str());
//fprintf(stdout, "length is %d\n", m_Stripchars.length() );
for (size_t i = 0; i < m_Stripchars.length(); i++){
        //fields.replace(m_Stripchars[i], "");
    string::size_type pos=0;
    while( (pos=fields.find(m_Stripchars[i], 0)) != string::npos)
    {
        fields.replace(pos, 1, "");
        //fprintf(stdout, "fields is %s\n", fields.c_str());
        //pos += 1;
   }
        //strField.erase ( remove ( strField.begin(), strField.end(), m_Stripchars[i] ),strField.end() ) ;
}
//fields.erase(
//remove_if(fields.begin(), fields.end(),
//bind2nd(equal_to<char>(), m_Stripchars[i])), fields.end());
utils::trim(fields);
//fprintf(stdout, "fields is %s\n", fields.c_str());
return fields;
}

/*
 self.nameprefixes = {s.lower() for s in self.nameprefixes}
 self.namesuffixes = {s.lower() for s in self.namesuffixes}
 self.maleprefixes = {s.lower() for s in self.maleprefixes}
 self.femaleprefixes = {s.lower() for s in self.femaleprefixes}
 */
//const MatcherNameAddress::m_consonants = "bcdfghjklmnpqrstvwxyz";
MatcherNameAddress::MatcherNameAddress(const string fields, const string postcodetolerance) :
MatcherExact(fields){

  if (postcodetolerance == ""){
  m_Postcodetolerance = 0;
  }
  else{
  m_Postcodetolerance = atoi(postcodetolerance.c_str()); // from string to int
  }

  //Initialize the constant member variable:
  vector < string> vNameprefixes(nameprefixes,
  nameprefixes + sizeof(nameprefixes) / sizeof(string));
  m_Nameprefixes = vNameprefixes;
  for_each(m_Nameprefixes.begin(), m_Nameprefixes.end(), utils::tolowerstr);

  vector < string> vNamesuffixes(namesuffixes,
  namesuffixes + sizeof(namesuffixes) / sizeof(string));
  m_Namesuffixes = vNamesuffixes;
  for_each(m_Namesuffixes.begin(), m_Namesuffixes.end(), utils::tolowerstr);

  vector < string> vMaleprefixes(maleprefixes,
  maleprefixes + sizeof(maleprefixes) / sizeof(string));
  m_Maleprefixes = vMaleprefixes;
  for_each(m_Maleprefixes.begin(), m_Maleprefixes.end(), utils::tolowerstr);

  vector < string> vFemaleprefixes(femaleprefixes,
  femaleprefixes + sizeof(femaleprefixes) / sizeof(string));
  m_Femaleprefixes = vFemaleprefixes;
  for_each(m_Femaleprefixes.begin(), m_Femaleprefixes.end(), utils::tolowerstr);

  vector < string> vCompanies(companies, companies + sizeof(companies) / sizeof(string));
  m_Companies = vCompanies;

  vector < string> vUnitprefixes(unitprefixes,
  unitprefixes + sizeof(unitprefixes) / sizeof(string));
  m_Unitprefixes = vUnitprefixes;

  vector < string> vRegionstopwords(regionstopwords,
  regionstopwords + sizeof(regionstopwords) / sizeof(string));
  m_Regionstopwords = vRegionstopwords;

  vector < string> vaddresspostfixes(addresspostfixes, addresspostfixes + sizeof(addresspostfixes) / sizeof(string));
 // fprintf(stdout, "size is %d\n", sizeof(addresspostfixes) / sizeof(string));
  m_Addresspostfixes = vaddresspostfixes;
  /*vector<string>::iterator iter = m_Femaleprefixes.begin();
   for(; iter != m_Femaleprefixes.end(); iter++)
   fprintf(stdout, "femaleprefixes: %s\n", (*iter).c_str());
   */

  //use  alg interface
  AlgInterface *interface = AlgInterface::GetPtr();
  distancefunc_nameaddress = (PAlgs*) (interface->algo_damerau_levenshtein_distance);
}

void
MatcherNameAddress::DeleteCompanies(vector<string> &value){

  size_t size = sizeof(m_Companies) / sizeof(string);
  vector<string>::iterator iter = value.begin();
  for(; iter != value.end(); iter++){
  for (size_t i = 0; i < size; i++){
          if((*iter).find(m_Companies[i]) != string::npos)
            {
              iter =  value.erase(iter);
            }
        }
  }
}


string
MatcherNameAddress::FindLongestMatch(const string &value, vector<string> &sequence, string stopchar){
  //fprintf(stdout, "value in FindLongestMatch:%s\n", value.c_str());
  vector<string>::iterator iter = sequence.begin();
  string strNameprefixes;
  vector < string > prefixes;
  string strPrefix;
  for (; iter != sequence.end(); iter++){
    strNameprefixes = (*iter);
    strNameprefixes += stopchar;
    if (value.find(strNameprefixes) == 0){
    //fprintf(stdout, "strNameprefixes:%s\n", strNameprefixes.c_str());
    prefixes.push_back((*iter));
    }
  }
  if (prefixes.size() > 0){
    sort(prefixes.begin(), prefixes.end());
    strPrefix = prefixes.back();
     //strPrefix += " ";
    //fprintf(stdout, "FindLongestMatch: strPrefix:%s\n", strPrefix.c_str());
  }

  return strPrefix;
}

void
MatcherNameAddress::NameFilter(const string &fields, vector<string> &names)
{
//fprintf(stdout, "enter MatcherNameAddress::NameFilter\n");
string value = fields;

 // joiniftuple(value);

//remove special chars like ".''"
string stipchars = ".''";
for (size_t i = 0; i < stipchars.length(); i++)
value.erase(
remove_if(value.begin(), value.end(), bind2nd(equal_to<char>(), stipchars[i])),
value.end());

utils::tolowerstr(value);

//if there's " and ", push the two into vector
while(value.find(" and ") != string::npos)
value = value.replace(value.find(" and "), 5, "&");
 //remove "and" and put into the vector: names
if (value.find("&") != string::npos)
{
size_t j, i = 0;
string fld;
do
{
j = utils::splitstring(value, fld, i, "&");
utils::trim(fld);
names.push_back(fld);
i = j + 1;
}
while (j < value.length());
}
else
{
names.push_back(value);
}

 //remove company names
DeleteCompanies(names);

vector < string > trimednames = names;

//trim prefixes
vector<string>::iterator iter = names.begin();
string strPrefix;
for (; iter != names.end(); iter++)
{

strPrefix = FindLongestMatch((*iter), m_Nameprefixes);
if (strPrefix != "")
trimednames.erase(remove(trimednames.begin(), trimednames.end(), strPrefix),
trimednames.end());
 //fprintf(stdout,"(*iter):%s\n", (*iter).c_str());
}

size_t k = trimednames.size();
//fprintf(stdout, "k:%d\n", k);
if (k > 1){
  vector<string> vect1, vect2;
  utils::splitstringbyspace(trimednames[0], vect1);
  utils::splitstringbyspace(trimednames[1], vect2);
  if (k == 2){
    if (vect1.size() == 0 && vect2.size() > 0){
        names[0] += " ";
        names[0] += trimednames[1];

    }
  else if (vect1.size() < vect2.size()){
  names[0] += " ";
  names[0] += vect2.back();
  }
//fprintf(stdout, "name0: %s\n", names[0].c_str());
}
else if (k == 3){
  vector < string > vect3;
  utils::splitstringbyspace(trimednames[2], vect3);
  if (vect1.size() == 0 && vect2.size() == 1 && vect3.size() == 2){
   //fprintf(stdout, "enter k = 3 case \n");
  names[0] += " ";
  names[0] += trimednames[1];
  names[0] += vect3.back();
  vector < string > vect4;
  utils::splitstringbyspace(names[1], vect4);
  names[2] = vect4.front() + " " + names[2];
  //delete the names[1]
  string tmp = names[1];
  names[1] = names[2];
  names.pop_back();
}
}
}

}

//split name as Initials/First Initital/First name/surename
void
MatcherNameAddress::SplitName(string &value, MetaData *metadata)
{

 //get the name prefix
string strPrefix = FindLongestMatch(value, m_Nameprefixes);
//fprintf(stdout, "SplitName:FindLongestMatch:%s\n", strPrefix.c_str());
if (strPrefix != "")
{
metadata->prefix = strPrefix;
metadata->nameweight += 1;
if (find(m_Maleprefixes.begin(), m_Maleprefixes.end(), strPrefix) != m_Maleprefixes.end())
metadata->gender = "male";
else if (find(m_Femaleprefixes.begin(), m_Femaleprefixes.end(), strPrefix)
!= m_Femaleprefixes.end())
metadata->gender = "female";

 //remove prefix:
size_t i = strPrefix.length();
value = value.substr(value.find(strPrefix) + i);
utils::trim(value);
}

//handle initials and first initial
list < string > components;
utils::splitstringbyspace(value, components);
string initials;
 //list<string>::iterator cmpIter = components.begin();
while (components.size() > 0)
{
if (components.front().length() == 1)
{
initials = initials + " " + components.front();
components.pop_front();
}
else
break;
}
utils::trim(initials);
if (initials != "")
{
metadata->initials = initials;
metadata->firstinitial = initials.substr(0, initials.find(" "));
}

// get the surename
if ((components.size() > 0))
{
metadata->surname = components.back();
components.pop_back();
}
if ((components.size() > 0))
{
list<string>::iterator comInter;
metadata->nameweight += 1;

//if there's no Initials for the name, and then substract the first Letter of each world as the Initial/first Initial
if (initials == "")
{
for (comInter = components.begin(); comInter != components.end(); comInter++)
{
initials += " ";
initials += (*comInter)[0];
}
utils::trim(initials);
metadata->initials = initials;
metadata->firstinitial = initials.substr(0, initials.find(" ")); //the first letter
}
// get the first name
string firstnames;
for (comInter = components.begin(); comInter != components.end(); comInter++)
{
firstnames += " ";
firstnames += (*comInter);
}
utils::trim(firstnames);
metadata->firstnames = firstnames;
metadata->firstname = components.front();
}
}

bool
cmp(const MetaData *a, const MetaData *b)
{ //升序排序时必须写的函数

return a < b;
}
void
MatcherNameAddress::RemoveDoubleconsonant(string &value)
{
  //fprintf(stdout, "Removedoubleconsonant with value is %s\n", value.c_str());
string consonant = "bcdfghjklmnpqrstvwxyz";

for (size_t i = 0; i < value.length() - 1; i++)
{
if (consonant.find(value[i]) != string::npos && value[i] == value[i + 1])
{
   // fprintf(stdout, "value before removedoubleconsonant is %s\n", value.c_str());
    value = value.erase(i, 1);

    //value.erase(remove(value.begin(), value.end(), value[i]), value.end());
break;
}
}
//fprintf(stdout, "RemoveDoubleconsonant: %s\n", value.c_str());
}

void
MatcherNameAddress::RemoveVowel(string &value)
{

string vowel = "aeiou";

for (size_t i = 0; i < value.length(); ++i)
{
if (vowel.find(value[i]) != string::npos)
{
   value = value.erase(i, 1);
    //value.erase(remove(value.begin(), value.end(), value[i]), value.end());
break;
}
}
//fprintf(stdout, "RemoveVowel: %s\n", value.c_str());

}
void
MatcherNameAddress::RemoveVowelall(string &value)
{
string vowel = "aeiou";
size_t i = value.find(vowel);
if (i != string::npos)
{
value = value.erase(i, 5);
}
//fprintf(stdout, "RemoveVowel: %s\n", value.c_str());
}

void
MatcherNameAddress::RemoveStopword(string &value)
{

 //nothing to do
}
void
MatcherNameAddress::IterateUntilmin(string &value, int minlen, Pfun func,
Pfun funcall)
{

string newvalue = value;
if (funcall != NULL)
{
funcall(value);
if (value.length() <= (size_t) minlen)
return;
}
value = newvalue;
while (true)
{
func(value);
if (newvalue == value || value.length() <= (size_t) minlen)
break;
newvalue = value;
}
}
string
MatcherNameAddress::Reduce(const string &value, int minlen)
{
  string result = value;
  //fprintf(stdout, "Reduce: value: %s\n", value.c_str());
  IterateUntilmin(result, minlen, MatcherNameAddress::RemoveDoubleconsonant);
  //fprintf(stdout, "Reduce: value: %s\n", result.c_str());
  IterateUntilmin(result, minlen, MatcherNameAddress::RemoveVowel,
  MatcherNameAddress::RemoveVowelall);
  //fprintf(stdout, "Reduce: value: %s\n", result.c_str());
  IterateUntilmin(result, minlen, MatcherNameAddress::RemoveStopword);
  //fprintf(stdout, "Reduce: value: %s\n", result.c_str());
  return result;
}
//get the surename
string
MatcherNameAddress::NameFilterfunc(const string &fields, MetaData *metadata)
{
//fprintf(stdout, "enter NameFilterfunc\n");
vector < string > names;
NameFilter(fields, names);
vector<string>::iterator nameIter = names.begin();
vector<MetaData *> vMetaDatas;
for (; nameIter != names.end(); nameIter++)
{
    //fprintf(stdout, "names after NameFilter is %s\n", (*nameIter).c_str());
MetaData *mtdata = new MetaData();
SplitName((*nameIter), mtdata);
vMetaDatas.push_back(mtdata);
}
if (vMetaDatas.size() > 1)
{
//fprintf(stdout, "vMetaDatas size bigger than 1\n");

 //sort by nameweight:
sort(vMetaDatas.begin(), vMetaDatas.end(), cmp);
vMetaDatas[0]->nameweight += 2;
}
 //metadata.update(splits[0])
metadata->firstinitial = vMetaDatas[0]->firstinitial;
metadata->initials = vMetaDatas[0]->initials;
metadata->gender = vMetaDatas[0]->gender;
metadata->prefix = vMetaDatas[0]->prefix;
metadata->firstnames = vMetaDatas[0]->firstnames;
metadata->firstname = vMetaDatas[0]->firstname;
metadata->surname = vMetaDatas[0]->surname;
metadata->postcode = vMetaDatas[0]->postcode;
metadata->nameweight = vMetaDatas[0]->nameweight;
//fprintf(stdout, "firstinitial:%s,initials:%s,gender:%s,prefix:%s,firstnames:%s,firstname:%s,surname:%s, postcode:%s,nameweight:%d\n",
//metadata->firstinitial.c_str(), metadata->initials.c_str(),
//metadata->gender.c_str(), metadata->prefix.c_str(),
//metadata->firstnames.c_str(), metadata->firstname.c_str(),
//metadata->surname.c_str(), metadata->postcode.c_str(), metadata->nameweight);
 //memcpy(metadata, vMetaDatas[0], sizeof(MetaData));

string surname;
if (vMetaDatas[0]->surname != "")
surname = vMetaDatas[0]->surname;
//fprintf(stdout, "before reduce:%s\n", surname.c_str());
string result = Reduce(surname, 3);


 //free MetaData
 //free MetaData
for (vector<MetaData *>::iterator it = vMetaDatas.begin();
it != vMetaDatas.end(); it++)
if (NULL != *it)
{
delete *it;
*it = NULL;
}
vMetaDatas.clear();
//fprintf(stdout, "leave NamefilterFunction\n");
return result;

}

//find unit, apartment or apt
int
MatcherNameAddress::GetUnitIndex(string &address)
{
int result = m_Unitprefixes.size();
int i;

if (address.find("/") != string::npos)
result = -1;
for (i = 0; i < result; i++){
    if (address.find(m_Unitprefixes[i]) == 0 ){
        if (i < result)
          result = i;
        break;
    }
  }
return result;

}

string
MatcherNameAddress::AddressFilterfunc(string &addr,
MetaData *metadata)
{
 // string stipchars = ".''";
 //for(int i = 0; i< stipchars.length(); i++)
 //result.erase(remove_if(result.begin(),result.end(), bind2nd(equal_to<char>(), stipchars[i])), result.end());
//fprintf(stdout, "enter function AddressFilterfunc, address is %s\n", addr.c_str());
string address = addr;
size_t j = 0;
size_t sizeofCompany = m_Companies.size();
vector<string>::iterator it;
string strField;
int i = 0, index = -1;
map<int, string> mapofSplits;
size_t pos;

do{
         //parse the field line, like: nameline==name, address1==address,
         j = utils::advplain(address, strField, i, ";");
         utils::trim(strField);
         //fprintf(stdout, "strField is %s\n", strField.c_str());
         //delete company:
         for (size_t k = 0; k < sizeofCompany; k++){
             if(strField.find(m_Companies[k]) != string::npos){
               //remove it
                 strField = "";
                 break;
             }
         }
         //handling the "street" and "road"

         for(it = m_Addresspostfixes.begin(); it != m_Addresspostfixes.end(); it++){
               string tmpBeforeReplace = (*it);
                string tmpAfterReplace = (*++it);
                if((pos = strField.find(tmpBeforeReplace)) != string::npos){
                          string tmp = strField;
                          strField = tmp.substr(0, pos) + tmpAfterReplace;// + strField.substr(pos+tmpBeforeReplace.length()-1);
                          if(tmp.length() > pos + tmpBeforeReplace.length()+1){
                              strField = strField + tmp.substr(pos+tmpBeforeReplace.length());
                          }
                      }
                  }
         //fprintf(stdout, "it's done\n");
         if(strField != ""){
           index = GetUnitIndex(strField);
           //fprintf(stdout, "index is %d\n", index);
           //mapofSplits.clear();
           mapofSplits.insert(make_pair(index, strField));
         }
          i = j + 1;
    }
   while (j < address.length());

//fprintf(stdout, "before insert into a map\n");
 //to sort when insert into a map:
string result;
string tmpString;
map<int, string>::iterator mapIter = mapofSplits.begin();
for (; mapIter != mapofSplits.end(); mapIter++)
{
result += " ";
tmpString = mapIter->second;
trim(tmpString);
result += tmpString;

}
//fprintf(stdout, "after insert into a map, resutl is %s\n", result.c_str());
result = result.substr(1); //remove " "
//fprintf(stdout, "result of Address before regular expreesion is %s\n", result.c_str());

 //regular expression
//fprintf(stdout, "begin regular expression match\n");
const boost::regex pattern1("unit[ ]{0,1}([0-9]){1,4}[,]{0,1}[ ]{0,1}(.*)");
const boost::regex pattern2("([0-9]{1,4})/([0-9]{1,4})-[0-9]{1,4} (.*)");
const boost::regex pattern3("u[ ]{0,1}([0-9]){1,4}[,]{0,1}[ ]{0,1}(.*)");
//const boost::regex pattern3("unit ")
if (regex_match(result, pattern1))
{
//fprintf(stdout, "does match pattern1\n");
    //fprintf(stdout, "result before replace is %s\n", result.c_str());
result = boost::regex_replace(result, pattern1, "\\1/\\2");

//fprintf(stdout, "result is %s\n", result.c_str());

size_t k = result.find("-");
//fprintf(stdout, "the rest of string is %s\n", (result.substr(result.find_first_of(" ", k)).c_str()));
if(k != string::npos)
  result  = result.substr(0, k ) + result.substr(result.find_first_of(" ", k));

//fprintf(stdout, "reslut after replace:%s\n", result.c_str());
}
else if (regex_match(result, pattern2))
{
//fprintf(stdout, "does match pattern2\n");
result = boost::regex_replace(result, pattern2, "\\1/\\2 \\3");

//fprintf(stdout, "reslut after replace:%s\n", result.c_str());
}
else if (regex_match(result, pattern3))
{
//fprintf(stdout, "does match pattern1\n");
    //fprintf(stdout, "result before replace is %s\n", result.c_str());
result = boost::regex_replace(result, pattern3, "\\1/\\2");

//fprintf(stdout, "result is %s\n", result.c_str());

size_t k = result.find("-");
//fprintf(stdout, "the rest of string is %s\n", (result.substr(result.find_first_of(" ", k)).c_str()));
if(k != string::npos)
  result  = result.substr(0, k ) + result.substr(result.find_first_of(" ", k));

//fprintf(stdout, "reslut after replace:%s\n", result.c_str());
}
utils::trim(result);
/*if(result != ""){
    finalresult += " ";
    finalresult += result;
}*/

//fprintf(stdout, "AddressFilterfunc, result is %s\n", finalresult.c_str());
return result;

}

string
MatcherNameAddress::PostcodeFilterfunc(const string &postcode,
MetaData *metadata)
{
string result = postcode;
string stipchars = ".''";
for (size_t i = 0; i < stipchars.length(); i++)
result.erase(
remove_if(result.begin(), result.end(),
bind2nd(equal_to<char>(), stipchars[i])), result.end());

//remove space
while(result.find(" ") != string::npos){
    utils::lTrim(result);
}

 //remove ","
if (result.find(",") != string::npos)
result.replace(result.find(","), 1, " ");
metadata->postcode = result;
result = "postcode" + result;

return result;

}
bool
MatcherNameAddress::IsRegionstopword(const string &value)
{
if (find(m_Regionstopwords.begin(), m_Regionstopwords.end(), value) != m_Regionstopwords.end())
{
return true;
}
else
return false;
}

string
MatcherNameAddress::RegionFilterfunc(const vector<string> &region, MetaData *metadata){

  vector < string > result = region;

  size_t j;
  string rst;

  list < string > splits;
  string rgn;

  const boost::regex pattern("[0-9]{4}");

  for (size_t i = 0; i < result.size(); i++){
      if (regex_match(result[i], pattern)){ // it's postcode{
          rgn = result[i];
          result.clear();
          result.push_back(rgn);
          //fprintf(stdout, "rgn:%s\n", rgn.c_str());
          metadata->postcode = rgn;
          break;
      }
      else{              //it's not postcode
          utils::splitstringbyspace(result[i], splits);
          list<string>::iterator spltsIter = splits.begin();
          while (spltsIter != splits.end()){
              if (IsRegionstopword(*spltsIter)){ //remove regionstopword
                  list<string>::iterator iter_e = spltsIter++;
                  splits.erase(iter_e);
              }
              else
                ++spltsIter;
          }

          //combine together for each element in splits
          string split;
          spltsIter = splits.begin();
          while (spltsIter != splits.end()){
              split += " ";
              split += (*spltsIter);
              ++spltsIter;
          }
          utils::trim(split);
          result[i] = split;
      }

  }

//combine together for each element in result
j = 0;
while (j < result.size())
{

if(result[j] != "")
  {
    rst += " ";
    rst += result[j];
  }

 ++j;
}
rst = rst.substr(1); // remove ","
//fprintf(stdout, "rst in RegionFilterfunc is%s\n", rst.c_str());
return rst;
}

string
MatcherNameAddress::GetMatchkey(const MapNoSort &fieldmap,
const vector<string> &data, MetaData *metadata)
{
//fprintf(stdout, "enter MatcherNameAddress::GetMatchkey\n");

vector<int>::const_iterator indexIter;
MapNoSort::const_iterator fieldmapIter;
string key;
string name;
string tmpData;

//handle name, first get the index of the data
for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end();
fieldmapIter++)
{
if (fieldmapIter->first == "name")
{
for (indexIter = fieldmapIter->second.begin();
indexIter != fieldmapIter->second.end(); indexIter++)
{
name += ",";
tmpData = data[*indexIter];
utils::trim(tmpData);
name += tmpData;
}
}

}
//fprintf(stdout, "name is %s\n", name.c_str());
//get the data
if (name != "" && utils::countOccurrences(name, ",") != name.length())
{
name = name.substr(1);
//fprintf(stdout, "name key:%s\n", name.c_str());
key = NameFilterfunc(name, metadata);
metadata->name = key;
//fprintf(stdout, "after name key:%s\n", key.c_str());
}
//fprintf(stdout, "after name key:%s\n", key.c_str());
//handle address
vector < string > address;
string addr;
string stipchars = ".''";
size_t pos;
for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end(); fieldmapIter++){
    if (fieldmapIter->first == "address"){
        //fprintf(stdout, " before for cycle \n");
        for (indexIter = fieldmapIter->second.begin(); indexIter != fieldmapIter->second.end(); indexIter++){

            tmpData = data[*indexIter];
            if(tmpData != ""){
                addr += ";";
                utils::trim(tmpData);
                addr += tmpData;
            }

        }
    }
}
  if(addr != ""){
        addr = addr.substr(1);

            //fprintf(stdout, "address is %s\n", addr.c_str());
            utils::tolowerstr(addr);
            for (size_t i = 0; i < stipchars.length(); i++){
                addr.erase(
                    remove_if(addr.begin(), addr.end(), bind2nd(equal_to<char>(), stipchars[i])), addr.end());
            }
           // fprintf(stdout, "address is %s\n", addr.c_str());
            pos = addr.find(",");
            if (pos != string::npos){
                addr.replace(pos, 1, ";");
                //address.push_back(addr.substr(0, pos));
                //address.push_back(addr.substr(pos + 1));
            }
            //else
            //  address.push_back(addr);*/


//if (address.size() > 0){
key += " ";
key += AddressFilterfunc(addr, metadata);
//fprintf(stdout, "after address, key:%s\n", key.c_str());

 //vKey += key;
//}
}

//handle postcode
string postcode;
for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end();
fieldmapIter++)
{
if (fieldmapIter->first == "postcode")
{
for (indexIter = fieldmapIter->second.begin();
indexIter != fieldmapIter->second.end(); indexIter++)
{
postcode += " ";
tmpData = data[*indexIter];
utils::trim(tmpData);
postcode += tmpData;

}
postcode = postcode.substr(1);
//fprintf(stdout, "postcode key:%s\n", postcode.c_str());
}
}
if (postcode != "")
{

key += " ";
key += PostcodeFilterfunc(postcode, metadata);

//fprintf(stdout, "after postcode, key:%s\n", key.c_str());
 //vKey += key;
}

//handle region
vector < string > region;
string rgn;

for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end(); fieldmapIter++){
    if (fieldmapIter->first == "region"){
        for (indexIter = fieldmapIter->second.begin(); indexIter != fieldmapIter->second.end(); indexIter++){
            rgn = data[*indexIter];
            utils::trim(rgn);
            utils::tolowerstr(rgn);
            for (size_t i = 0; i < stipchars.length(); i++)
              rgn.erase(remove_if(rgn.begin(), rgn.end(), bind2nd(equal_to<char>(), stipchars[i])), rgn.end());
            pos = rgn.find(",");
            if (pos != string::npos){
                region.push_back(rgn.substr(0, pos));
                region.push_back(rgn.substr(pos + 1));
            }
            else
              region.push_back(rgn);

        }
    }
  }
  if (region.size() > 0){
      key += " ";
      key += RegionFilterfunc(region, metadata);
      //vKey += key;
  }

//fprintf(stdout, "after region, key:%s\n", key.c_str());
//handle data:
string strdata = "";
for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end();
fieldmapIter++)
{
if (fieldmapIter->first == "data")
{
for (indexIter = fieldmapIter->second.begin();
indexIter != fieldmapIter->second.end(); indexIter++)
{

strdata += ",";
tmpData = data[*indexIter];
utils::trim(tmpData);
strdata += tmpData;

}
}
}
if (strdata != "")
{
strdata = strdata.substr(1);
utils::tolowerstr(strdata);
key += " ";
key += strdata;
}

//add gender
if(name != "" || strdata != ""){
  key += " ";
  key += "gender";
  if (metadata->gender != "")
  {
  key += metadata->gender;
  }

  //add n
  char result[20] = { 0 };
  int i = 99 - metadata->nameweight;
  sprintf(result, "nameweight%02d", i);
  key += " ";
  key += result;
}
//fprintf(stdout, "leave GetMatchKey() and key is %s\n", key.c_str());
return key;
}
bool
MatcherNameAddress::Compare(string value1, string value2, MetaData *meta1,
MetaData *meta2)
{
  string v1 = value1;
  string v2 = value2;

 // fprintf(stdout, "value1 is %s, value2 is %s\n", value1.c_str(), value2.c_str());
  v1 = v1.substr(0, v1.find("gender") - 1);
  v2 = v2.substr(0, v2.find("gender") - 1);

  bool result;
  if (m_Postcodetolerance > 0){
      v1 = v1.substr(0, v1.find("postcode") - 1);
      v2 = v2.substr(0, v2.find("postcode") - 1);
      result = MatcherExact::Compare(v1, v2, meta1, meta2);

      if (result){
          if (abs(
              (atoi(meta1->postcode.c_str()) - atoi(meta2->postcode.c_str()))) > m_Postcodetolerance)
            result = (meta1->postcode == meta2->postcode);
      }
      return result;
  }
  else{
    groupofreturns returns;
    gropofparams params;
    params.str1 = meta1->name.c_str();
    params.str2 = meta2->name.c_str();

    //fprintf(stdout, "before distancefunc, name1 is %s, name2 is %s\n", params.str1, params.str2);
    //clock_t  nTimeStart = clock();
    returns = ((PAlgs) distancefunc_nameaddress)(&params);
    //clock_t  nTimeEnd = clock();
   // m_time += (nTimeStart - nTimeEnd);
    //fprintf(stdout, "time used: %f\n", (double)m_time);
    double distance = returns.i;
    //fprintf(stdout, "distance is %f \n", distance);
     //int distance = distancefunc(value1.c_str(), value2);
    //fprintf(stdout, "v1 is %s, v2 is %s\n", v1.c_str(), v2.c_str());
    if(v1.length() > meta1->name.length())
    v1 = v1.substr(meta1->name.length() + 1);
    else v1 = "";
    if(v2.length() > meta2->name.length())
    v2 = v2.substr(meta2->name.length() + 1);
    else v2 = "";

    //fprintf(stdout, "after: v1 is %s, v2 is %s\n", v1.c_str(), v2.c_str());
    return (distance <= 2 && MatcherExact::Compare(v1, v2, meta1, meta2));
  }
  //result = MatcherExact::Compare(v1, v2, meta1, meta2);


}
string
MatcherIndividual::NameFilterfunc(const string &fields, MetaData *metadata)
{

string result = MatcherNameAddress::NameFilterfunc(fields, metadata);

if (metadata->firstinitial != "") //add firstinitial as the key
result = result + " " + metadata->firstinitial;
return result;

}
bool
MatcherIndividual::Compare(string value1, string value2, MetaData *meta1,
MetaData *meta2)
{
bool result = MatcherNameAddress::Compare(value1, value2, meta1, meta2);
if (meta1->firstname != "" && meta2->firstname != "" && result) //if the two records all have first name, they have to be compared
result = (meta1->firstname == meta2->firstname);

return result;
}
bool
MatcherIndividualPrefix::Compare(string value1, string value2, MetaData *meta1,
MetaData *meta2)
{
bool result = MatcherIndividual::Compare(value1, value2, meta1, meta2);
//fprintf(stdout, "result is  %d, gender of 1 is %s, gender of 2 is %s\n", result, meta1->gender.c_str(), meta2->gender.c_str() );
if (meta1->gender != "" && meta2->gender != "" && result)
{
result = (meta1->gender == meta2->gender);
}
return result;
}
MatcherClose::MatcherClose(const string fields, const string tolerance,
const string algorithm) :
MatcherExact(fields)
{
if(tolerance == "") m_Tolerance = 0;
else m_Tolerance = atof(tolerance.c_str());
//fprintf(stdout, "m_Tolerance is %f, algorithm is %s\n", m_Tolerance, algorithm.c_str());

//use  alg interface
AlgInterface *interface = AlgInterface::GetPtr();
filterfunc = (PAlgs*) (interface->algo_simplefilter);
distancefunc = (PAlgs*) (interface->algo_damerau_levenshtein_distance);

//handle algorithm
m_Algorithm = algorithm;
//distance algorithm: hamming, jaro, jaro-winkler, levenshtein, damerau-levenshtein and hall
if (m_Algorithm == "jaro")
distancefunc = (PAlgs*) interface->algo_jaro_distance;
else if (m_Algorithm == "jaro-winkler")
distancefunc = (PAlgs*) interface->algo_jaro_winkler;
else if (m_Algorithm == "hamming")
distancefunc = (PAlgs*) interface->algo_hamming_distance;
else if (m_Algorithm == "levenshtein")
distancefunc = (PAlgs*) interface->algo_levenshtein_distance;
else if (m_Algorithm == "damerau-levenshtein")
distancefunc = (PAlgs*) interface->algo_damerau_levenshtein_distance;
else if (m_Algorithm == "hall")
{
distancefunc = (PAlgs*) interface->algo_hall_distance;
filterfunc = (PAlgs*) interface->algo_hall_filter;
}
else if (m_Algorithm == "metaphone") //match algorithm
{
m_Tolerance = 0;
distancefunc = (PAlgs*) interface->algo_simpledistance;
filterfunc = (PAlgs*) interface->algo_metaphone;
}
else if (m_Algorithm == "soundex")//match algorithm
{
m_Tolerance = 0;
distancefunc = (PAlgs*) interface->algo_simpledistance;
filterfunc = (PAlgs*) interface->algo_soundex;
}
else if (m_Algorithm == "nysiis")//match algorithm
{
m_Tolerance = 0;
distancefunc = (PAlgs*) interface->algo_simpledistance;
filterfunc = (PAlgs*) interface->algo_nysiis;
}
else if (m_Algorithm == "match-rating-codex")//match algorithm
{
m_Tolerance = 0;
distancefunc = (PAlgs*) interface->algo_simpledistance;
filterfunc = (PAlgs*) interface->algo_match_rating_codex;
}
else
{
//fprintf(stdout, "the algorithm is beyond of this application\n");
//defauts to damerau-levenshtein"
distancefunc = (PAlgs*) interface->algo_damerau_levenshtein_distance;
}

}
char*
MatcherClose::SimpleFilter(const char *str1)
{
  //fprintf(stdout, "enter simplerFilter\n");
string value = str1;
//fprintf(stdout, "before trim, value is %s\n", value.c_str());
utils::trim(value);
//fprintf(stdout, "before tolowerstra, value is %s\n", value.c_str());
utils::tolowerstr(value);
//fprintf(stdout, "before copy, value is %s\n", value.c_str());
char *result = (char*) malloc((value.length()+1) * sizeof(char));
memset(result,0,sizeof(result));
strcpy(result, value.c_str());
//fprintf(stdout, "result of simpleFilter is %s\n", value.c_str());
return result;

//return (char*)value.c_str();
}
int
MatcherClose::SimpleDistance(const char *str1, const char *str2)
{
return strcmp(str1, str2);
}

int
MatcherClose::HallDistance(const char *str1, const char *str2)
{
char *hallvalue1 = HallFilter(str1);
char *hallvalue2 = HallFilter(str2);
int distance = damerau_levenshtein_distance(hallvalue1, hallvalue2);
//fprintf(stdout, "the distance is %d\n", distance);
free(hallvalue1);
free(hallvalue2);
return distance;
}

char*
MatcherClose::HallFilter(const char *str1)
{
  //fprintf(stdout, "enter HallFilter, input data is %s\n", str1);
string value = str1;
string stipchars = ",. ";
for (size_t i = 0; i < stipchars.length(); i++)
value.erase(remove_if(value.begin(), value.end(), bind2nd(equal_to<char>(), stipchars[i])),value.end());
utils::tolowerstr(value);
while(value.find(" ") != string::npos)
  {
    value =  value.erase(value.find(" "));
  }

sort(value.begin(), value.end());
//fprintf(stdout, "after before sort, the value is %s\n", value.c_str());
char *rst = (char*) malloc((value.length() + 1) * sizeof(char));
memset(rst,0,sizeof(rst));
strcpy(rst, value.c_str());
//fprintf(stdout, "the result of HallFilter:%s\n", value.c_str());
return rst;
}

string
MatcherClose::GetMatchkey(const MapNoSort &fieldmap, const vector<string> &data,
MetaData *metadata)
{
//fprintf(stdout, "Enter GetMatchkey\n");
groupofreturns returns = {0};
gropofparams params = {0};
vector<int>::const_iterator indexIter;
MapNoSort::const_iterator fieldmapIter;
string strdata, strlist;
for (fieldmapIter = fieldmap.begin(); fieldmapIter != fieldmap.end(); fieldmapIter++){
  if (fieldmapIter->first == "data"){
    for (indexIter = fieldmapIter->second.begin(); indexIter != fieldmapIter->second.end(); indexIter++){
        //fprintf(stdout, "Enter into cycle\n");
        strdata = data[*indexIter];
        //fprintf(stdout, "strdata is %s\n", strdata.c_str());
        //params->str1 = (char*)malloc(strdata.length()*sizeof(char*));
        params.str1 = strdata.c_str();
        returns = ((PAlgs) filterfunc)(&params);
       // fprintf(stdout, "returns is %s\n", returns.j);

        if( returns.j != NULL)
          {
            string result = returns.j;
            //fprintf(stdout, "result is %s\n", result.c_str());
            free(returns.j);

            strlist += " ";
            strlist += result;
          }
        //fprintf(stdout, "strlist is%s\n", strlist.c_str());
    }
  }

}
if (strlist != "")
  {
    strlist = strlist.substr(1);
    //fprintf(stdout, "result is %s\n", strlist.c_str());
  }
return strlist;

}
bool
MatcherClose::Compare(string value1, string value2, MetaData *meta1,
MetaData *meta2)
{
groupofreturns returns;
gropofparams params;
params.str1 = value1.c_str();
params.str2 = value2.c_str();
returns = ((PAlgs) distancefunc)(&params);
double distance = returns.i;
//fprintf(stdout, "distance is %f and m_Tolerance is%f\n", distance, m_Tolerance);
 //int distance = distancefunc(value1.c_str(), value2);
if (m_Tolerance > 0 && distance <= m_Tolerance)
return true;
else
return (value1 == value2);
}
//AlgInterface* AlgInterface::m_instance=NULL;

