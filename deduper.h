#ifndef DEDUPER_H_
#define DEDUPER_H_
#endif /* DEDUPER_H_ */

//#include <stdlib.h>
#include <list>
#include <map>
#include <string>
#include<fstream>
#include<vector>
#include"datafile.h"
//#include "Hash.h"
//#include"matcherexact.h"

#include <time.h>
//#define 0 (void *(0))
//#define NULL 0

using namespace std;


//class DataFile;
class MatcherExact;
class RecordsHash;
//class DedupeError {
  //DedupeError(){}
  //~DedupeError(){}
//};

struct MetaData{
  string gender;
  string prefix;
  string initials;
  string firstinitial;
  string firstnames;
  string firstname;
  string surname;
  string postcode;
  string state;
  string name;
  int nameweight;

  MetaData(){
    nameweight = 0;
  }
  bool   operator <   (const   MetaData *a ){  //��������ʱ����д�ĺ���

       return   nameweight   <   a->nameweight;
  }
};
struct FileRecords{
  string flag;
  string matchkey;
  vector<string> vdata;
  DataFile *datafile;
  MetaData *mtdata;
  FileRecords(){

    datafile = NULL;
    mtdata = NULL;
  }
  ~FileRecords(){
    datafile = NULL;
    mtdata = NULL;
  }
};

class Deduper {
public:
	Deduper();
	~Deduper();

	//get the value from the config file according to the keyname:
	string GetConfigOption(const char *sectionname, const char *keyname, ifstream& readfile, const char *prefix = NULL);

	//set MatcherExact object, this object is already created before
	void SetMatcher(MatcherExact *pMatcher);

	//set the hash table point
	void SetHashofData(RecordsHash *pRecordsHash){m_HashofData = pRecordsHash; }

	//get the MatcherExact object point
	MatcherExact * GetMatcher(){return m_pMatcher;}

	//set suppressiononly Flag:
	void SetSuppressiononlyFlag(bool bFlag){m_Suppressiononly = bFlag;}

	void SetPriority(string strPriority);

	//read data from the file, create match key and insert data into hash table
	void AddData(DataFile * datafile, const string &data);

	void FlagGroup();
	void SetTotaldropcount(){m_TotalDropCount += 1;}
	void SetTotalretaincount(){m_TotalRetainCount += 1;}
	static const  map<string, string> m_MapofTypeFlag;
	string filenameize(string name, string suffix = "",  string ext = ".csv");

	string *m_Dropmatrix;
        list<DataFile*> m_ListofDataFile; //list of data files objects
        //vector<FileRecords*> m_Data;
        RecordsHash *m_HashofData; //hashtable is for match
        vector<string> m_Outputfieldnames;
        string m_strOutputfilednames;
        DataFile* m_OutputFile;



	private:
        int m_TotalRetainCount;
        int m_TotalDropCount;
        bool m_Suppressiononly;
        MatcherExact *m_pMatcher;
        string m_Priority;

        //get the value with the section and key from the config file
        string GetOption(const string &strSection, const string &strKey, ifstream& readfile);

	//DedupeError *m_pDedupeError;
	
};
