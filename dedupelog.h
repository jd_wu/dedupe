/*
 * DedupeLog.h
 *
 *  Created on: 2013-6-12
 *      Author: min
 */

#ifndef DEDUPELOG_H_
#define DEDUPELOG_H_

#include<string>
#include<fstream>
using namespace std;
class DedupeLog
{
public:

  ~DedupeLog();

private:
  DedupeLog(string LogFileName);
  static DedupeLog *m_instance;
  ofstream *m_pf;
  static string m_logfilename;

public:

  static DedupeLog* GetPtr(string LogFileName){

    if(LogFileName != "")
    m_logfilename = LogFileName;

      if(m_instance == NULL){
          m_instance = new DedupeLog(LogFileName);
      }

      return m_instance;
  };

      static DedupeLog* GetPtr(){

          if(m_instance == NULL){
              m_instance = new DedupeLog(m_logfilename);
          }

      return m_instance;
  };

//  DedupeLog& WriteLog(string message);
 // DedupeLog& operator<<(string message);
  //DedupeLog& operator<<(const char *message);

  void WriteLog(string message);


};

#endif /* DEDUPELOG_H_ */
