#ifndef __HASH__H__
#define __HASH__H__
#endif
#include <string.h>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;
class Deduper;
struct FileRecords;

typedef pair<string, int> PAIR;

struct CmpByValue {
  bool operator()(const PAIR& lhs, const PAIR& rhs) {
    return lhs.second < rhs.second;
  }
};
// specified sort
struct CStrMultiMapNoSort{
    bool operator() (const string& str1, const string& str2) const{
        return true;
    }
  };
//map without sort:
typedef multimap<string, string, CStrMultiMapNoSort> StrMultiMapNoSort;


typedef struct qnode_t
{
    struct qnode_t *prev;
    struct qnode_t *next;
    FileRecords *data;
} qnode_t;

typedef struct qc_t {
    qnode_t *head;
    qnode_t *tail;
    qc_t *next;
    int size;
    int prioritypoint;
    bool suppressionflag;
    bool isPset;
    bool isCrossFiles;
    bool isPRSet;
    bool isRSSet;
    char *filename;



} qc_t;

class RecordsHash
{

public:
   /* Hash(int qsize)
    {
        _qsize = qsize;
        _pq = (qc_t *)malloc(_qsize * sizeof(qc_t));
        memset(_pq,0,_qsize*sizeof(qc_t));
    };*/
    RecordsHash(Deduper *deduper);
    ~RecordsHash(void);
    /* insert an element into hash queue */
    /* if success,return 0,else return -1 */
    int HashInsertElement(FileRecords *pqe);
   // void SetDropcountforSuppression();
    //void SetDropcountforInternaldupes();
   // void SetDropcountforCrossfiles();
    void SetFlagForSuppressionOnly();
    //void SetRetaincountforSuppression();
    void SetRetainAndDropFlag();

    //set priority
    void SetPriorityCondition(string &priority);

    void WriteOutputfile(bool readall, ofstream &writefile, bool fields);
    void WriteMatchesFile(bool readall, bool fields, string& matchesfile);
    void WriteDropstofile(string &dropfile);

   // double m_time;
private:
    Deduper *m_pDeduper;
    //int hashKey(FileRecords *pqe);
    bool CmpFunc(FileRecords *pqe1,FileRecords *pqe2);

    void CheckFieldsValueForEqual();

    int CalculatePriorityPoints(qnode_t *pnode);


    bool FindFileNamewithMinValue(string& filenamelist , string& filename);
    void SetValueForFile(string& filename, int i = 1);

    //int _qsize;        /* hash key maximum size */
    qc_t *m_pPq;         /* pointer to hash queue */
    StrMultiMapNoSort m_priorityfields;
    bool m_priorityhasequal; //equal flag
    map<string, int> m_retainedtimesforeachfile;
    //vector<int> m_prioritystatus;


};

