#include"utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <functional>
#include   <cctype>
#include <iterator>
#include <sstream>
using namespace std;

namespace utils
{

  FILE *fp;

  int
  splitstring(const string& str, string& fld, int i, const string &sep){

    string::size_type j;

    j = str.find_first_of(sep, i); // look for streparator

    if (j == string::npos){ // none found

        j = str.length();
      }
    fld = str.substr(i, j - i);
    return j;
  }

  int
  getArrayLen(string &array){
    return (sizeof(array) / sizeof(array[0]));
  }

  string&
  lTrim(string &ss){
    string::iterator p = find_if(ss.begin(), ss.end(),
        not1(ptr_fun<int, int>(isspace)));
    ss.erase(ss.begin(), p);
    return ss;
  }

  string&
  rTrim(string &ss){
    string::reverse_iterator p = find_if(ss.rbegin(), ss.rend(),
        not1(ptr_fun<int, int>(isspace)));
    ss.erase(p.base(), ss.end());
    return ss;
  }

  string&
  trim(string &st){
    lTrim(rTrim(st));
    return st;
  }

  string& trimAndreduceWhiteSpace(string &str, string::size_type pos)
  {
      static const string delim = " "; //remove space in string
      pos = str.find_first_of(delim, pos);
      if (pos == string::npos)
          return str;
      return trimAndreduceWhiteSpace(str.erase(pos, 1));
  }

  string &
  rTrimComma(string &array) {
    //É¾³ýÄ©Î²¶ººÅ
    array = array.substr(1);
    return array;
  }

  string::size_type countOccurrences(const string &str, const string &substr)
  {
    int nCount = 0;

     // string::size_type substrSize = substr.size();
      string::size_type idxSub = str.find(substr, 0);
      while (idxSub!=string::npos) {
         ++nCount;
         ++idxSub;
         idxSub = str.find(substr, idxSub);
      }
      return nCount;
  }
  //split the original string into vector by space
  void
  splitstringbyspace(const string &str, vector<string> &vstr){

    stringstream strstrm(str);
    vstr.assign(istream_iterator < string > (strstrm),
        istream_iterator<string>());
  }
  void
  splitstringbyspace(const string &str, list<string> &lstr){
    stringstream strstrm(str);
    lstr.assign(istream_iterator < string > (strstrm),
        istream_iterator<string>());
  }

  //read filed from fixed length file
  void
  splitfixedlenfile(vector<string>& field, const string &line,
      const vector<int> &fieldlength, const int recordlength){

    vector<int>::const_iterator filedlenIter = fieldlength.begin();
    string::size_type i = 0;
    string fld;
    for (; filedlenIter != fieldlength.end() /*&& i < line.length()*/ ; filedlenIter++){
      fld = line.substr(i, (*filedlenIter));
      //È¥µô¿Õ¸ñ
      //trim(fld);
     // fprintf(stdout, "fields of fixedlen, i: :%s, %d, %d\n", fld.c_str(), i, *filedlenIter);
      field.push_back(fld);
      i += *filedlenIter;
    }
}
int
getindexofvector(const vector<string> &fieldnames, const string &str){
  //fprintf(stderr, "enter getindexofvector\n");
  int index = 0;
  vector<string>::const_iterator iter = fieldnames.begin();
  for (; iter != fieldnames.end(); iter++){
      //fprintf(stderr, "string in vector is %s, str is %s\n", (*iter).c_str(), str.c_str());
    if ((*iter) == str)
      return index;
    index++;
  }
return -1;

}
 // split: split line into fields which is delimited by separator
void
splitcsv(vector<string>& field, const string &line, const string sep, const char quote){

  //fprintf(stdout, "enter splitcsv, line is %s\n", line.c_str());
  string fld;
string::size_type i, j;
//char separtor = <char>(sep);

if (line.length() == 0)
  return;
i = 0;

//fprintf(stdout, "strFieldDelimiter is %s\n", strFieldDelimiter.c_str());
do{
    if (i < line.length() && quote == line[i] )
      j = advquoted(line, fld, ++i, quote); // skip quote
    else if(i<line.length())
      j = advplain(line, fld, i, sep);
    else
      {
        field.push_back("");
        return;

      }

    trim(fld);
    field.push_back(fld);
    i = j + 1;
  }
while (j < line.length());

}

 // advquoted: quoted field; return index of next separator
int
advquoted(const string& str, string& fld, int i, const char quote){
 //fprintf(stdout, "enter advauoted\n");
string::size_type j;
//string tmp;
fld = quote;
for (j = i; j < str.length(); j++){
    if (str[j] == quote)//&& str[++j] != quote)
      {
       // fprintf(stdout, "the string is %s\n", tmp.c_str());
        fld += quote;
        j++;
        break;
    }
    else
    fld += str[j];

}
//fld = tmp ;

return j;
}

 //get the string before the separator, eg. there's a line like nameline=name, address1=address, and this
 //function can get nameline=name for the first separtor"," which is default separator
int
advplain(const string& str, string& fld, int i, const string sep){
//fprintf(stdout, "enter advplain, sep is %s\n", sep.c_str());
string::size_type j;

j = str.find_first_of(sep, i); // look for separator

if (j == string::npos){ // none found

  j = str.length();
}
//fprintf(stdout, "str is %s, i is %d, j is %d \n", str.c_str(), i, j);
fld = str.substr(i, j-i);
//fprintf(stdout, "fld is %s\n", fld.c_str());
return j;
}

void
tolowerstr(string &str){

for (string::size_type i = 0; i < str.length(); ++i){
/*if(str[i] >= 'a' && str[i] <= 'z')
 {
 str[i] = str[i] - 32;
 }
 else */if (str[i] >= 'A' && str[i] <= 'Z') {
    str[i] = str[i] + 32;
  }
else {
    continue;
  }
}
}
//escape the character
string
unestrcape(const string& str){
//fprintf(stdout, "str:%s\n", str.c_str());
string dst;
char c;
string::const_iterator it = str.begin();
while (it != str.end()){
c = *it++;
if (/*c == '\\' && */it != str.end()){
    switch (*it++){
    case '\\':
      c = '\\';
      break;
    case 'n':
      c = '\n';
      break;
    case 't':
      c = '\t';
      break;
    case '\'':
      c = '\'';
      break;
      // all other estrcapestr
    default:
      // invalid estrcape strequence - strkip it. alternatively you can copy it astr istr, throw an exception...
      continue;
      }
  }
dst += c;
}

//fprintf(stdout, "str:%s\n", dst.c_str());
return dst;
}


void
stripquote(string &str, const char quote){
  //fprintf(stdout, "before strip quote: %s\n", str.c_str());
while (str.find_first_of(quote) != string::npos) {
    //fprintf(stdout, "doing strip quote: %s\n", str.c_str());
      str = str.erase(str.find_first_of(quote), 1);
    }

    while (str.find(",") != string::npos){
        //str.erase(str.find(" "));
        str = str.replace(str.find(","), 1, " ");
     }
  if(str.find("  ") != string::npos)
  str = str.replace(str.find("  "), 2, " ");
}
void replaceTabtoCommo(string &str){
         string tmp = str;
         string newStr;
         string::size_type k = 0;
        while((k = tmp.find_first_of('\t')) != string::npos){
             //fprintf(stdout, "find tab\n");
            newStr += ",";
            newStr += tmp.substr(0, k);
            tmp = tmp.substr(k+1);

            // strData = strData.replace(strData.find('\t'), 1, ",");

           }
        newStr += ",";
        newStr += tmp;
        newStr = newStr.substr(1);
        str = newStr;
}

void CreateFixedLengthString(string& InputString, const string& FieldLength){
  //fprintf(stdout, "enter CreateFixedLengthString\n");
  size_t k = 0;
  size_t m = 0;
  unsigned int iFiledLen = 0;
  unsigned int index = 0;
  string Inputdata = InputString;
  string data, tmp;
  string Fieldlen = FieldLength;
  while((k = Inputdata.find_first_of(",")) != string::npos && (m = Fieldlen.find_first_of(",")) != string::npos){

      iFiledLen = atoi(Fieldlen.substr(0, m).c_str());
      //fprintf(stdout, "fieldlen is %d, k is %d\n", iFiledLen, k);
      if(iFiledLen > k+1){
          while(index < iFiledLen-k-1)
            {
              data += " ";
              index++;
            }
      }


      index = 0;
      Fieldlen = Fieldlen.substr(m+1);
      tmp = Inputdata.substr(0, k);
     trim(tmp);
     data += tmp;
     //fprintf(stdout, "data is %s\n", data.c_str());
      Inputdata = Inputdata.substr(k + 1);
   }
   index = 0;
   iFiledLen = atoi(Fieldlen.c_str());
   if(iFiledLen >Inputdata.length()+1){
     while( index < iFiledLen-Inputdata.length()-1){
          data += " ";
          index++;
      }

   }

   data += Inputdata;

   InputString = data;

}
}
