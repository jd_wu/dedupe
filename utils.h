/*
 * utils.h
 *
 *  Created on: 2013-5-30
 *      Author: min
 */

#ifndef UTILS_H_
#define UTILS_H_


#include<string>
#include<vector>
#include<list>

using namespace std;



/*
string unescape(const string& s)
{
  string res;
  string::const_iterator it = s.begin();
  while (it != s.end())
  {
    char c = *it++;
    if (c == '\\' && it != s.end())
    {
      switch (*it++) {
      case '\\': c = '\\'; break;
      case 'n': c = '\n'; break;
      case 't': c = '\t'; break;
      // all other escapes
      default:
        // invalid escape sequence - skip it. alternatively you can copy it as is, throw an exception...
        continue;
      }
    }
    res += c;
  }

  return res;
}
*/


namespace utils {




  extern int splitstring(const string& str, string& fld, int i, const string &sep = ",");
  extern int getArrayLen(string &array);
  extern string&  lTrim(string   &ss);
  extern string&  rTrim(string   &ss);
  extern string&   trim(string   &st);
  extern string& trimAndreduceWhiteSpace(string &str, string::size_type pos = 0);
  extern string & rTrimComma(string &array);
  extern string::size_type countOccurrences(const string &str, const string &substr);
  extern void splitstringbyspace(const string &str, vector<string> &vstr);
  extern void splitstringbyspace(const string &str, list<string> &lstr);
  extern void splitfixedlenfile(vector<string>& field, const string &line, const vector<int> &fieldlength, const int recordlength);
  extern int getindexofvector(const vector<string> &fieldnames,  const string &str);
  extern    void splitcsv(vector<string>& field, const string &line, const string sep = ",", const char quote = '"' );
  extern int advquoted(const string& str, string& fld, int i, const char quote = '"');
  extern int advplain(const string& str, string& fld, int i, const string sep = ",");

  extern void tolowerstr(string &str);
  extern string unestrcape(const string& str);

  extern void stripquote(string &str, const char quote = '"' );
  extern void replaceTabtoCommo(string &str);

  extern void CreateFixedLengthString(string& InputString, const string& FieldLength);
}
#endif /* UTILS_H_ */
