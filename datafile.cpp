/*
 * DataFile.cpp
 *
 *  Created on: 2013-5-21
 *      Author: min
 */
#include <stdlib.h>
#include "deduper.h"
//#include"DataFile.h"
#include <stdlib.h>
#include <stdio.h>
#include<string.h>
#include<cctype>
#include"matcherexact.h"
#include"dedupelog.h"

#include"utils.h"

//using namespace utils;
DataFile::DataFile(Deduper *deduper, const string filename, const string filetype) {


        if(deduper != NULL){
            m_pDeduper = deduper;

            //find the filetype: like mail, suppression, retainsubst, retainplus and output
            map < string, string >::const_iterator iter = m_pDeduper->m_MapofTypeFlag.find(filetype);
            if(iter != m_pDeduper->m_MapofTypeFlag.end()){
                m_Filetype = filetype;
            }
            else{
                for(iter = m_pDeduper->m_MapofTypeFlag.begin(); iter != m_pDeduper->m_MapofTypeFlag.end(); iter++){
                    if(iter->second == filetype){
                        m_Filetype = iter->first;
                        break;
                    }
                }
            }
            m_Filename = filename;
            //m_Filepath = filepath;
            //if(m_Filename != ""){
            //    if(m_Filename.find("\\") != string::npos){

             //   }
           // }
            m_Count = 0;
            m_Retaincount = 0;
            m_Dropcount = 0;
            m_Fileorder = 0;
         //m_pfileData = (FileData *)malloc(sizeof(FileData));
         //memset(m_pfileData,0, sizeof(FileData));
    }
}
int DataFile::SetFieldnames(const string &fieldnames, string delimiter){
  string strnames = fieldnames;
 // if(delimiter == "") delimiter = ",";//set the default delimiter for header is ","
  //utils::tolowerstr(strnames);
 //fprintf(stderr, "strname is %s, delimter is %s\n", strnames.c_str(), delimiter.c_str());
/* if(strnames.find(",") == string::npos && strnames.find("\t") != string::npos)
   {
     utils::replaceTabtoCommo(strnames);
   }*/
// fprintf(stderr, "strname after change is %s\n", strnames.c_str());
  //handle the files with separtor of ''\t'
// fprintf(stdout, "delimiter is %s\n", delimiter.c_str());
  utils::splitcsv(m_Fieldnames, strnames, delimiter);
  /*vector<string>::const_iterator iter = m_Fieldnames.begin();
    for (; iter != m_Fieldnames.end(); iter++){
        fprintf(stderr, "fieldnames is %s\n", (*iter).c_str());
    }*/
  if(m_pDeduper->GetMatcher()->GetFieldMap(m_Fieldmap, m_Fieldnames) < 0)
    return -1;

 // fprintf(stdout, "strnames:%s\n",strnames.c_str());
  //to lower string:

  if(m_Filetype != "suppression")
    {
    string headerwithcomma = strnames ;
  if(delimiter != ",")
    {
     while (headerwithcomma.find(delimiter) != string::npos){
                  //str.erase(str.find(" "));
         headerwithcomma = headerwithcomma.replace(headerwithcomma.find(delimiter), 1, ",");
     }
    }

    m_pDeduper->m_strOutputfilednames = headerwithcomma;
    //transform(strfieldnames.begin(),strfieldnames.end(),strfieldnames.begin(),tolower);
    if(m_pDeduper->m_Outputfieldnames.size() == 0){
        m_pDeduper->m_Outputfieldnames = m_Fieldnames;
    }
    }
//debug:
/*map<string, vector<int> >::iterator iter;
vector<int>::iterator vInter;
for(iter = m_Fieldmap.begin(); iter != m_Fieldmap.end(); iter++){
    fprintf(stdout, "m_GetFieldMap:%s\n", iter->first.c_str());
    for(vInter = iter->second.begin(); vInter != iter->second.end(); vInter++){
        fprintf(stdout, "vector:%d\n", (*vInter));
    }
}*/
return 0;

}
void DataFile::SetOutputfieldmap(const vector<string> &outputfieldnames){

  DedupeLog::GetPtr()->WriteLog("enter setoutputGetFieldMap:\n");
  int index = 0;
  //char strIndex[10];
  vector<string>::const_iterator outputfieldnamesIter = outputfieldnames.begin();
  vector<int> vIndex;
  for(; outputfieldnamesIter != outputfieldnames.end(); outputfieldnamesIter++){
          if((index = utils::getindexofvector(m_Fieldnames, (*outputfieldnamesIter))) >= 0){
              m_Outputfieldmap.push_back(index);
          }
          //TODO: erro handling
         // else fprintf(stderr, "error: setoutputGetFieldMap(): Required output field is not in input file");
  }
}
void DataFile::AddAll(const string &data){

  if(data != ""){
  m_pDeduper->AddData(this, data);
  m_Count +=1;
  }
}

void DataFile::SetMatrixDropcount(string filename){
  //fprintf(stdout, "enter SetMatrixDropcount, filename is %s\n", filename.c_str());

  if(m_mapofMatrixDropcount.find(filename) != m_mapofMatrixDropcount.end())
    {
      m_mapofMatrixDropcount[filename] += 1;
    }
  else{

      //fprintf(stdout, "enter else\n");
      m_mapofMatrixDropcount.insert(make_pair(filename, 1));
      //fprintf(stdout, "leave else\n");
  }
}
void DataFile::SetTotalMatrixDropcount(string filename, int totalnumber){
  //fprintf(stdout, "enter SetMatrixDropcount, filename is %s\n", filename.c_str());

  if(m_mapofMatrixDropcount.find(filename) != m_mapofMatrixDropcount.end())
    {
      m_mapofMatrixDropcount[filename] += totalnumber;
    }
  else{

      //fprintf(stdout, "enter else\n");
      m_mapofMatrixDropcount.insert(make_pair(filename, totalnumber));
      //fprintf(stdout, "leave else\n");
  }
}
DataFileDelimited::DataFileDelimited(Deduper *deduper, const string filename, const string filetype, const string recorddelimiter, const string fielddelimiter, const string quotedelimiter)
                            :DataFile(deduper, filename, filetype ){
        //need decode:
      //fprintf(stdout, "filename:%s, filetype:%s, m_Recorddelimiter:%s, m_Fielddelimiter: %s, m_Quotedelimiter:%s\n"
      //    ,filename.c_str(),filetype.c_str(), m_Recorddelimiter.c_str(),m_Fielddelimiter.c_str(), m_Quotedelimiter.c_str());
      m_Recorddelimiter = recorddelimiter;
      m_Fielddelimiter = fielddelimiter;
      m_Quotedelimiter = quotedelimiter;

      //length check
      if(recorddelimiter == ""){
          //raise error: 'Record delimiter must be at least one character'
          fprintf(stderr, "error: Record delimiter must be at least one character\n");
          string msg = "erro: Record delimiter must be at least one character\r\n";
          DedupeLog::GetPtr()->WriteLog(msg);
      }
      if(fielddelimiter == ""){
          //raise error: 'Field delimiter must be one character'
          fprintf(stderr, "erro: Field delimiter must be at least one character\n");
          string msg = "erro: Field delimiter must be at least one character\r\n";
          DedupeLog::GetPtr()->WriteLog(msg);
      }
      if(quotedelimiter == ""){
          //raise error: 'Quote delimiter must be one character'
          fprintf(stderr, "erro: Quote delimiter must be at least one character\n");
          string msg = "erro: Quote delimiter must be at least one character\r\n";
          DedupeLog::GetPtr()->WriteLog(msg);
      }

}

//Read delimited file
 int DataFileDelimited::DataFileDelimited::ReadAll(){

   //fprintf(stdout, "enter ReadAll\n");
  ifstream readDelimitedfile;
  //construct the file path
  //fprintf(stdout, "filename:%s\n", m_Filename.c_str());

  readDelimitedfile.open(m_Filename.c_str(),ios::in);
  if(!readDelimitedfile.is_open()){
      readDelimitedfile.close();
       fprintf(stderr, "error: cannot open the file specified %s\n", m_Filename.c_str());
       string msg = "error: cannot open the file specified  " + m_Filename + "\r\n";
       DedupeLog::GetPtr()->WriteLog(msg);
       return -1;
   }
  readDelimitedfile.seekg (0, ios::end);
  if(readDelimitedfile.tellg() == 0){
      fprintf(stderr, "error: File(%s) does not contain an even number of records\n", m_Filename.c_str());
      string msg = "error: File  " + m_Filename + " does not contain an even number of records" + "\r\n";
      DedupeLog::GetPtr()->WriteLog(msg);
      readDelimitedfile.close();
      return -1;
  }
  readDelimitedfile.clear();
  readDelimitedfile.seekg(0, ios::beg);
  //read file data into data
  string data;

//fprintf(stdout, "headrecord is %s, m_Fielddelimiter is %s\n", m_Headerrecord.c_str(), m_Fielddelimiter.c_str());
   if(m_Headerrecord == "true" || m_Headerrecord == "skip"){
          getline(readDelimitedfile,data);
          if(m_Headerrecord == "true"){

              if(m_Fielddelimiter == "\\t"){
                  utils::replaceTabtoCommo(data);
                  if(SetFieldnames(data) < 0)
                                   return -1;
              }
              else{
                if(SetFieldnames(data, m_Fielddelimiter) < 0)
                  return -1;
              }


         }
          else  if (SetFieldnames(m_Header) < 0)
            return -1;

      }
      else if (SetFieldnames(m_Header, m_Fielddelimiter) < 0)
        return -1;

  while(!readDelimitedfile.eof()){

      getline(readDelimitedfile,data);
      if(data != "")
        {
         // if(m_Fielddelimiter == "\\t"){
         //     utils::replaceTabtoCommo(data);
         // }
         // fprintf(stdout, "data:%s before AddAll\n", data.c_str());
          AddAll(data); // call the AddAll function of the Base Class
        }

      //m_pDeduper->adddata(this, data);
      //m_count +=1;
  }
  readDelimitedfile.close();
 // fprintf(stdout, "leave ReadAll\n");
  return 0;


}

DataFileFixedLength::DataFileFixedLength(Deduper *deduper, const string filename, const string filetype,const string recordlength, const string fieldlengths)
:DataFile(deduper, filename, filetype ){

  //fprintf(stdout, "filename:%s, filetype:%s, recordlength:%s, fieldlengths: %s\n",filename.c_str(),filetype.c_str(), recordlength.c_str(),fieldlengths.c_str());
  if(recordlength != "")  m_Recordlength = atoi(recordlength.c_str());
  m_strFieldlengths = fieldlengths;
      //parse fieldlenghts, like 7,4,32,15,60,31, store each number into a vector<int>
      int sumoffieldlen  = 0;
      //int fieldlen = 0;
      string strFieldlen = fieldlengths;
      //size_t comma = 0;
      /*while(strFieldlen != ""){
          comma = strFieldlen.find(',', 0);
          if(comma != string::npos){
              fieldlen = atoi(strFieldlen.substr(0, comma).c_str());
              sumoffieldlen += fieldlen;
              m_Fieldlengths.push_back(fieldlen);
              strFieldlen = strFieldlen.substr(comma+1);
             // fprintf(stdout, "strFieldlen:%s\n", strFieldlen.c_str());
          }
          else{

              utils::trim(strFieldlen);
              fieldlen = atoi(strFieldlen.c_str());
              sumoffieldlen += fieldlen;
              m_Fieldlengths.push_back(atoi(strFieldlen.c_str()));
              break;
          }
      }*/
      vector<string> fieldlens;
      int len = 0;
      utils::splitcsv(fieldlens, strFieldlen);
      vector<string>::const_iterator iter = fieldlens.begin();
        for (; iter != fieldlens.end(); iter++){
            //fprintf(stderr, "fieldlen is %s\n", (*iter).c_str());
           len = atoi((*iter).c_str());
           sumoffieldlen += len;
            m_Fieldlengths.push_back(len);
        }
      if(sumoffieldlen != m_Recordlength){
          fprintf(stderr, "error: Sum of field lengths (%d) does not match record length (%d)", sumoffieldlen, m_Recordlength);
          char buf[210];
          sscanf(buf, "error: Sum of field lengths (%d) does not match record length (%d)", &sumoffieldlen, &m_Recordlength);
          DedupeLog::GetPtr()->WriteLog(buf);
      }

}

//read fixed length file
int DataFileFixedLength::DataFileFixedLength::ReadAll(){

      ifstream readFixedlenfile;
      //construct the file path
      //need to handle filename:
     // fprintf(stdout, "filename:%s\n", m_Filename.c_str());
      readFixedlenfile.open(m_Filename.c_str()/*"\\input\\"*/,ios::in);
      if(!readFixedlenfile.is_open()){
          readFixedlenfile.close();
          fprintf(stderr, "error: cannot open the file specified %s\n", m_Filename.c_str());
          string msg = "error: cannot open the file specified  " + m_Filename + "\r\n";
          DedupeLog::GetPtr()->WriteLog(msg);
           return -1;
       }
      readFixedlenfile.seekg (0, ios::end);

     float m = float(readFixedlenfile.tellg()) /m_Recordlength;
     float n = float(readFixedlenfile.tellg() / m_Recordlength);
      if(m != n){
          fprintf(stderr, "error: File(%s) does not contain an even number of records\n", m_Filename.c_str());
          string msg = "error: File  " + m_Filename + " does not contain an even number of records" +  "\r\n";
          DedupeLog::GetPtr()->WriteLog(msg);
          readFixedlenfile.close();
          return -1;
      }
      readFixedlenfile.clear();
      readFixedlenfile.seekg(0, ios::beg);
      //read file data into data
        char data[m_Recordlength];
        memset(data,'\0',sizeof(data));

        if(m_Headerrecord == "true" || m_Headerrecord == "skip"){
                readFixedlenfile.getline(data, m_Recordlength);
                if(m_Headerrecord == "true"){
                    SetFieldnames(data);
                }
                else  SetFieldnames(m_Header);

         }
        else{
                SetFieldnames(m_Header);
         }

        while(!readFixedlenfile.eof()){

            readFixedlenfile.getline(data, m_Recordlength);
            //if(stcmp(data, "") == 0) continue;
            AddAll(data);              // call the AddAll function of the Base Class
            //m_pDeduper->adddata(this, str);
            //m_count +=1;
        }
        readFixedlenfile.close();
        return 0;
}
