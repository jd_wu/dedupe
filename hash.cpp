/*
 * Hash.cpp
 *
 *  Created on: 2013-6-6
 *      Author: min
 */

#include"hash.h"

#include"deduper.h"
#include"matcherexact.h"
#include"utils.h"
#include"dedupelog.h"

#define FILE_NAME_LIST_SIZE 1000

RecordsHash::RecordsHash(Deduper *deduper)
{
  //_qsize = MAX_HASH_QUEUE_SIZE;
  //_qsize = 0;
  m_pPq = NULL;
  m_pDeduper = deduper;
  //m_pPq = malloc(_qsize * sizeof(qc_t));
  //memset(m_pPq,0,_qsize*sizeof(qc_t));

  m_priorityhasequal = false;

  //m_time = 0;
}

RecordsHash::~RecordsHash()
{
  if (m_pPq)
    {
      /* free all the allocated memory */
      qc_t *pqc = m_pPq;
      while (pqc != NULL)
        {

          qnode_t *pnode;
          pnode = pqc->head;
          while (pnode)
            {
              qnode_t *p0;
              p0 = pnode;
              pnode = pnode->next;
              if (p0->data)
                free(p0->data);
              free(p0);
            }
          if(pqc->filename != NULL){
              free(pqc->filename);
          }
          pqc = pqc->next;
        }
      free(m_pPq);
    }
}

int
RecordsHash::HashInsertElement(FileRecords *pqe)
{
  //fprintf(stdout, "Enter hashInsertElement\n");
 // clock_t nTimeStart = clock();
  DedupeLog::GetPtr()->WriteLog("enter hashInsertElement\r\n");
  qc_t *pqc, *p;
  qnode_t *pos, *pnode;
  if (pqe == NULL
    ) return -1;

  if ((pnode = (qnode_t *) malloc(sizeof(qnode_t))) == NULL)
    {
      fprintf(stderr, "error: memeory limited\n");
      DedupeLog::GetPtr()->WriteLog("error: memeory limited\n");
      return -1;
    }
  FileRecords *fileRecords = new FileRecords();
  pnode->data = fileRecords;

  pnode->next = NULL;
  //fprintf(stdout, "before assignement\n");
  //memcpy(pnode->data,pqe,sizeof(FileRecords));
  pnode->data->datafile = pqe->datafile;
  pnode->data->vdata = pqe->vdata;
  pnode->data->mtdata = pqe->mtdata;
  pnode->data->flag = pqe->flag;
  pnode->data->matchkey = pqe->matchkey;
  // if(pnode->data->flag == "RS" || pnode->data->flag == "RP"){
  //     pnode->data->flag = "R";
  //  }

  //   for(int i = 0; i< pnode->data->vdata.size(); i++)
  //   fprintf(stdout, "data in hashtable: %s\n", pnode->data->vdata[i].c_str());

  bool SetToP = false;
  string filenameofHead, filenamofCurr, filenamelist;
  int prioritypoint = 0;

  if (m_pPq != NULL)
    {
      pqc = m_pPq;
      while (pqc != NULL)
        {
          pos = pqc->head;
          if (pos != NULL)
            {
             // fprintf(stdout, "before compare\n");
              if (CmpFunc(pos->data, pqe))
                {
                  //fprintf(stdout, "after compare\n");
                 //handle RS, PR
                 //find the first RS record and keep it
                 if ((!pqc->isRSSet && pqe->flag == "RS") || (!pqc->isPRSet && pqe->flag == "RP")){
                     if(pqe->flag == "RP")  pqc->isPRSet = true;
                     else{
                         pqc->isRSSet  = true;
                     }
                     pqe->datafile->SetRetaincount();
                     m_pDeduper->SetTotalretaincount();
                     pqe->flag = "K";

                 }
                 //handle priority:
                 if(!pqc->isRSSet && !pqc->suppressionflag){
                     //fprintf(stdout, "before check\n");
                   /*SetToP = CheckFileldsForFileName(pnode);
                   FieldsCheck = CheckFieldsValueForPriority(pos, pnode);
                   if(m_priorityfilename != "" && m_priorityfields.size() >0){
                       SetToP =  (SetToP && FieldsCheck);
                   }
                   else if(m_priorityfields.size() >0) SetToP = FieldsCheck;*/
                     prioritypoint = CalculatePriorityPoints(pnode);
                    // fprintf(stdout, "prioritypoint is %d\n", prioritypoint);
                     if(prioritypoint > pqc->prioritypoint){
                        // fprintf(stdout, "before: pqc->prioritypoint is %d\n", pqc->prioritypoint);
                         SetToP = true;
                         pqc->prioritypoint = prioritypoint;
                         //fprintf(stdout, "after: pqc->prioritypoint is %d\n", pqc->prioritypoint);
                     }
                     pqc->isPset = SetToP;

                   //fprintf(stdout, "after check, SetToP is %d\n", SetToP);
                 }
                /* if(SetToP){
                     filename = pqe->datafile->GetFilename();
                     SetValueForFile(filename);????????????????????????????
                  }*/

                 //handle suppression
                 if(pnode->data->flag == "S"){
                        pqc->suppressionflag = true;
                        pqc->isPset = false;
                 }
                  if ((pos->data->mtdata->gender == ""
                      && pqe->mtdata->gender != "" && pqe->flag == "M")
                      || (pos->data->flag != "M" && pqe->flag == "M") || SetToP)
                    {
                      //fprintf(stdout, "exchange first node\n");
                      //exchange with the first node
                      pnode->data->datafile = pos->data->datafile;
                      pnode->data->vdata = pos->data->vdata;
                      pnode->data->mtdata = pos->data->mtdata;
                      pnode->data->flag = pos->data->flag;
                      pnode->data->matchkey = pos->data->matchkey;

                      pos->data->datafile = pqe->datafile;
                      pos->data->vdata = pqe->vdata;
                      pos->data->mtdata = pqe->mtdata;
                      pos->data->flag = pqe->flag;
                      pos->data->matchkey = pqe->matchkey;
                    }

                  //handle priority with "equal"
                  //collect different file names
                  if(m_priorityhasequal && !pqc->isPRSet && !pqc->suppressionflag && !pqc->isPset && pqe->flag == "M"){
                      if(pqc->filename == NULL){
                          pqc->filename = (char*)malloc(sizeof(char)*FILE_NAME_LIST_SIZE);
                          memset(pqc->filename, 0, sizeof(char)*FILE_NAME_LIST_SIZE);
                      }
                      filenamelist = pqc->filename;
                      filenameofHead = pos->data->datafile->GetFilename();
                      if(filenamelist.find(filenameofHead) == string::npos){
                          filenamelist += ",";
                          filenamelist += filenameofHead;
                      }
                      filenamofCurr = pnode->data->datafile->GetFilename();
                      if(filenamelist.find(filenamofCurr) == string::npos){
                          filenamelist += ",";
                          filenamelist += filenamofCurr;
                      }
                      //fprintf(stdout, "filenamelist is %s\n", filenamelist.c_str());
                      strncpy(pqc->filename, filenamelist.c_str(), filenamelist.length());
                   //fprintf(stdout, "filenamelist is %s\n", filenamelist.c_str());
                 }
                  while (pos != NULL) //reverse to the end of the element
                    {
                      //mark other nodes as "D" because this node is "S"
                      if((pqc->suppressionflag || pqc->isRSSet) && pos->data->flag == "M"){
                          pos->data->flag = "D";
                          pos->data->datafile->SetDropcount();
                          m_pDeduper->SetTotaldropcount();

                          pos->data->datafile->SetMatrixDropcount(
                              pnode->data->datafile->GetFilename());
                      }
                      if(pos->next == NULL){
                          break;
                      }
                        pos = pos->next;
                        pqc->size += 1;

                    }

                  pos->next = pnode;
                  pnode->prev = pos;
                  pqc->size += 1;


                  break;
                }
            }
          p = pqc;
          pqc = pqc->next;
        }
      if (pqc == NULL)
        {
          pqc = (qc_t *) malloc(sizeof(qc_t));
          memset(pqc, 0, sizeof(qc_t));
          p->next = pqc;

        }
    }
  else
    {
      m_pPq = (qc_t *) malloc(sizeof(qc_t));
      memset(m_pPq, 0, sizeof(qc_t));
      pqc = m_pPq;
    }
  if (pqc->head == NULL)
    {
      pqc->head = pqc->tail = pnode;
      pqc->next = NULL;
      pqc->size = 1;
      pqc->prioritypoint = CalculatePriorityPoints(pnode);
    }
 // clock_t nTimeEnd = clock();
  //m_time += (nTimeEnd - nTimeStart)/1000;
 // fprintf(stdout, "time to insert is %f\n", (double)(nTimeEnd - nTimeStart)/1000);
  //fprintf(stdout, "leave hashInsertElement\n");
  DedupeLog::GetPtr()->WriteLog("leave hashInsertElement\r\n");
  return 0;
}

void
RecordsHash::SetFlagForSuppressionOnly(){
 // fprintf(stdout, "enter SetFlagForSuppressionOnly\n");
  DedupeLog::GetPtr()->WriteLog("enter SetFlagForSuppressionOnly\r\n");
  if (m_pPq == NULL
    ) return;
  qc_t *pqc = m_pPq;
  qnode_t *pnode;
  //qnode_t *q = NULL;
  //int totalnumbers = 0;
  while (pqc != NULL){
      pnode = pqc->head;
      if (pqc->size > 1 && !pqc->suppressionflag){

              while (pnode != NULL) {
                  if (pnode->data->flag == "M"){
                      pnode->data->flag = "K";
                      pnode->data->datafile->SetRetaincount();
                      m_pDeduper->SetTotalretaincount();
                    }
                  pnode = pnode->next;
                }
      }
      else if (pqc->size == 1)
        {
          if (pnode->data->flag != "S" && pnode->data->flag != "RP"
              && pnode->data->flag != "RS")
            {
              pnode->data->datafile->SetRetaincount();
              m_pDeduper->SetTotalretaincount();
              pnode->data->flag = "K";

            }
        }
      pqc = pqc->next;
    }
  DedupeLog::GetPtr()->WriteLog("leave SetFlagForSuppressionOnly\r\n");
}

void
RecordsHash::SetRetainAndDropFlag()
{
  //fprintf(stdout, "enter SetRetainAndDropFlag\n");
  DedupeLog::GetPtr()->WriteLog("enter SetRetainAndDropFlag\r\n");
  if (m_pPq == NULL ) return;
  qc_t *pqc = m_pPq;
  qnode_t *pnode;
  qnode_t *firstrecordfrommail = NULL;

  CheckFieldsValueForEqual();

  while (pqc != NULL)
    {
      pnode = pqc->head;
      if (pqc->size > 1 && !pqc->suppressionflag && !pqc->isRSSet)
        {
          firstrecordfrommail = pqc->head;
          string filename = "";
          string filenamelist = "";
          string currfilename = "";
          string filenameinlist = "";
          bool isPSet = false;
          while (pnode != NULL){
                  //handle priority, find the filename with Minimum value
                  if(m_priorityhasequal && !pqc->isPset){
                      if(pqc->filename != NULL)
                      filename = pqc->filename;
                      if(FindFileNamewithMinValue(filename, filenameinlist)){
                        pqc->isPset = true;
                    }
                  }
                  if(filenameinlist != "" && !isPSet){
                      currfilename = pnode->data->datafile->GetFilename();
                      if(currfilename == filenameinlist){
                         pnode->data->flag = "K";
                         pnode->data->datafile->SetRetaincount();
                         m_pDeduper->SetTotalretaincount();
                         firstrecordfrommail = pnode;
                         isPSet = true;
                         SetValueForFile(filenameinlist);
                      //fprintf(stdout, "drop file: %s and filenamelist is %s\n", filenameinlist.c_str(), filenamelist.c_str());
                     }
                  }
                  //delete duplicates internally and cross multiple files
                  if (pnode != firstrecordfrommail && pnode->data->flag == "M")
                    {
                     // fprintf(stdout, "delete duplicates internally\n");
                      pnode->data->flag = "D";
                      pnode->data->datafile->SetDropcount();
                      m_pDeduper->SetTotaldropcount();
                      if(filenameinlist != "")
                        pnode->data->datafile->SetMatrixDropcount(filenameinlist);
                      else
                        pnode->data->datafile->SetMatrixDropcount(
                          firstrecordfrommail->data->datafile->GetFilename());
                    }
                  pnode = pnode->next;
                }
              //handle first node
                if (!isPSet)
                  { //set the first record from M as keep
                          firstrecordfrommail->data->datafile->SetRetaincount();
                          //fprintf(stdout, "set the first record\n");
                          m_pDeduper->SetTotalretaincount();
                          firstrecordfrommail->data->flag = "K";

                  }
                else if(isPSet && pqc->head != firstrecordfrommail ){//
                    pqc->head->data->flag = "D";
                    pqc->head->data->datafile->SetDropcount();
                    m_pDeduper->SetTotaldropcount();
                    pqc->head->data->datafile->SetMatrixDropcount(
                        firstrecordfrommail->data->datafile->GetFilename());
                }
              filenameinlist = "";
              isPSet = false;

            }
          else if (pqc->size == 1)
              {
                if (pnode->data->flag != "S" && pnode->data->flag != "RP"
                    && pnode->data->flag != "RS")
                  {
                    pnode->data->datafile->SetRetaincount();
                    m_pDeduper->SetTotalretaincount();
                    pnode->data->flag = "K";

                  }
              }
      pqc = pqc->next;

    }
  //fprintf(stdout, "leave SetRetainAndDropFlag\n");
  DedupeLog::GetPtr()->WriteLog("leave SetRetainAndDropFlag\r\n");
}

void RecordsHash::SetPriorityCondition(string &priority){
  if(priority == "") return;
  string localpriority = priority;
//the case without "equal"
   string strField, strFieldName, strFieldValue, tmp;
   size_t j, k, i = 0;
   map<string, string>::iterator fieldsIter;
   do{
         //parse the field line, like: nameline==name, address1==address,
         j = utils::advplain(localpriority, strField, i);
         utils::trim(strField);
         k = strField.find("==");
         if (k != string::npos)
         { //if there's "=", then get the fieldname and filedtype
             strFieldName = strField.substr(0, k);
             strFieldValue = strField.substr(k + 2, j - 1);
             utils::trim(strFieldName);
             utils::trim(strFieldValue);
             /*if(strFieldName == "filename"){
                 m_priorityfilename = strFieldValue;
             }
             else{*/
                /* if((fieldsIter = m_priorityfields.find(strFieldName)) != m_priorityfields.end()){
                     tmp = fieldsIter->second;
                     strFieldValue = tmp + "," + strFieldValue;
                     fieldsIter->second = strFieldValue;
                 }
                 else */m_priorityfields.insert(make_pair(strFieldName, strFieldValue)); //insert m_Fields member attribute
               // fprintf(stdout, "strFieldName: %s, strFieldValue: %s\n", strFieldName.c_str(), strFieldValue.c_str());
            // }


         }
         else { //if there's no "==" then check if it's only equal
             if(localpriority.find("equal") != string::npos){
                 m_priorityhasequal = true;
             }
          }
          i = j + 1;
    }
   while (j < localpriority.length());
}

int RecordsHash::CalculatePriorityPoints(qnode_t *pnode){
  int result = 0;
  if(m_priorityfields.size() >0 && pnode->data->flag == "M")
     {
      StrMultiMapNoSort::iterator fieldsIter = m_priorityfields.begin();
       //size_t sizeofm_priorityfields = m_priorityfields.size();
       int i = 0;
       int index =-1;
       string filename;
       //int i = -1;
       for(;fieldsIter != m_priorityfields.end(); fieldsIter++){
           //fprintf(stdout, "i is %d\n", i);
           //tmpStatus[j] = -1; //initiate the value;
          //fprintf(stdout, "enter for cycle\n");
           if(fieldsIter->first == "filename"){
               filename = pnode->data->datafile->GetFilename();
               if (filename.find(fieldsIter->second) != string::npos){
                   result += (int)pow(2, i);
                  // fprintf(stdout, "result in filename is %d, i is %d\n", result, i );
               }
           }
           if((index = utils::getindexofvector(pnode->data->datafile->GetFieldnames(), fieldsIter->first)) >= 0){
               if(fieldsIter->second == pnode->data->vdata[index]){
                   result += (int)pow(2, i);
                   //fprintf(stdout, "result is %d, i is %d\n", result, i );
               }
           }
           i++;
       }
 }
  //fprintf(stdout, "result is %d\n", result);
  return result;
}

void RecordsHash::CheckFieldsValueForEqual(){
  if (m_pPq == NULL ) return;
   qc_t *pqc = m_pPq;
   qnode_t *pnode;

   string filename;
   if(!m_priorityhasequal) return;

   while (pqc != NULL)
     {
        pnode = pqc->head;
       //calculate the
       if (pqc->size > 1 && pqc->isPset && !pqc->suppressionflag && !pqc->isRSSet)
         {
           filename = pnode->data->datafile->GetFilename();
           SetValueForFile(filename);
         }
       pqc = pqc->next;
     }

}
void RecordsHash::SetValueForFile(string& filename, int i){
  map<string, int>::const_iterator fieldmapIter;
  int times = 0;
  if ((fieldmapIter = m_retainedtimesforeachfile.find(filename)) != m_retainedtimesforeachfile.end() ){
        times = fieldmapIter->second;
        times += i;
        m_retainedtimesforeachfile[filename] = times;
 }
 else{
     if(i > 0)
     times = i;
     else times = 0;
     m_retainedtimesforeachfile.insert(make_pair(filename, times)); //insert filename and times to retain
   }
}

//find the filename with minimum value from the m_retainedtimesforeachfile
bool RecordsHash::FindFileNamewithMinValue(string& filenamelist, string& filename){
  string localfilenamelist = filenamelist;
  if(localfilenamelist.find(",") == 0)
    localfilenamelist = localfilenamelist.substr(1);

  //fprintf(stdout, "localfilenamelist is %s\n", localfilenamelist.c_str());

  if(localfilenamelist.find(",") == string::npos) return false;

  vector<PAIR> filename_vec;

  int times = 0, i = 0;
  size_t j = 0;
  string strField;
  map<string, int>::iterator fieldmapIter;
  vector<PAIR>::iterator iter;
  do{
            //parse the field line, like: nameline=name, address1=address,
        j = utils::advplain(localfilenamelist, strField, i);
        if ((fieldmapIter = m_retainedtimesforeachfile.find(strField)) != m_retainedtimesforeachfile.end() ){
                times = fieldmapIter->second;
        }
        else times = 0;
        i = j + 1;
        filename_vec.push_back(make_pair(strField, times));

  }
  while (j < localfilenamelist.length());
  sort(filename_vec.begin(), filename_vec.end(), CmpByValue());
  iter = filename_vec.begin();
  filename = iter->first;
  return true;
  //fprintf(stdout, "filename is %s\n", filename.c_str());
}
void
RecordsHash::WriteOutputfile(bool readall, ofstream& writefile, bool fields)
{
  DedupeLog::GetPtr()->WriteLog("enter WriteOutputfile\r\n");
  if (m_pPq == NULL
    ) return;
  qc_t *pqc = m_pPq;
  qnode_t *pnode;


  string strfields;
  string strHeader = m_pDeduper->m_OutputFile->GetHead();
  string strFinalHeader;
  string strFieldDelimiter;
  string strQuoteDelimiter;
  string strRecordDelimiter;
  string strFieldlength;

  //write header:

  DataFileDelimited* delimitedfile =
      dynamic_cast<DataFileDelimited*>(m_pDeduper->m_OutputFile);
  if (delimitedfile != NULL)
    {
      strFieldDelimiter = delimitedfile->GetFielddelimiter();
      strQuoteDelimiter = delimitedfile->GetQuotedelimiter();
      //fprintf(stdout, "strFieldDelimiter is %s\n", strFieldDelimiter.c_str());
      strRecordDelimiter = delimitedfile->GetRecorddelimiter();

      size_t k = 0;
      while ((k = strHeader.find(",")) != string::npos)
        {
          strFinalHeader += strFieldDelimiter;
          strFinalHeader += strHeader.substr(0, k);
          strHeader = strHeader.substr(k + 1);
        }
      strFinalHeader += strFieldDelimiter;
      strFinalHeader += strHeader;
      strFinalHeader = strFinalHeader.substr(strFieldDelimiter.length());

    }
  //fprintf(stdout, "enter fixedlength\n");
  DataFileFixedLength *fixedlenfile =
      dynamic_cast<DataFileFixedLength*>(m_pDeduper->m_OutputFile);
  if (fixedlenfile != NULL)
    {
      strRecordDelimiter = fixedlenfile->GetRecorddelimiter();
      strFieldlength = fixedlenfile->GetFieldLenghtsforString();
      strFieldDelimiter = ",";
      strFinalHeader = strHeader;
      string strHeaderPrefix;
      if (fields)
        {
          size_t index = 0;
          int i = 0;
          for (; index < strHeader.length(); index++)
            {
            if (strHeader[index] == ',')
              {
                i++;
              }
            if (i == 4)
              {
                strHeaderPrefix = strHeader.substr(0, index);
                strFinalHeader = strHeader.substr(index + 1);
                break;
              }
          }

        //fprintf(stdout, "strFinalHeader is %s\n", strFinalHeader.c_str());
      }
    //create fixedlength string
    utils::CreateFixedLengthString(strFinalHeader, strFieldlength);
    if (strHeaderPrefix != "")
      strFinalHeader = strHeaderPrefix + strFinalHeader;

  }
//fprintf(stdout, "strFinalHeader after FixedLength is %s\n", strFinalHeader.c_str());
writefile << strFinalHeader << endl;

vector<int>::iterator indexIter;
string data;
while (pqc != NULL)
  {

    pnode = pqc->head;
    while (pnode != NULL)
      {
        if (pnode->data->flag == "K" || (readall && pnode->data->flag == "D"))
          {
           // fprintf(stdout, "there's data for keep and drop\n");
            //fprintf(stdout, "size of outputfiledmap:%d\n", outputfiledmap.size());
            vector<int> outputfiledmap =
                pnode->data->datafile->GetOutputfiledmap();
            for (indexIter = outputfiledmap.begin();
                indexIter != outputfiledmap.end(); indexIter++)
                  {
              strfields += strFieldDelimiter;
              data = pnode->data->vdata[*indexIter];
              if(data.find(strFieldDelimiter) != string::npos)
                {
                  data = strQuoteDelimiter  + data + strQuoteDelimiter;
                }
              //fprintf(stdout, "Index is %d\n", *indexIter);
              strfields += data;
              //fprintf(stdout, "strfields is %s\n", strfields.c_str());
            }
           // fprintf(stdout, "strfields is %s\n", strfields.c_str());
          if (strfields != "" && utils::countOccurrences(strfields, strFieldDelimiter) != strfields.length())
            strfields = strfields.substr(strFieldDelimiter.length());

          if (fixedlenfile != NULL)
            {
              //create fixedlength string
              //fprintf(stdout, "strfields is %s\n", strfields.c_str());
              utils::CreateFixedLengthString(strfields, strFieldlength);
            }

          if (fields)
            writefile << pnode->data->datafile->GetFilename()
                << strFieldDelimiter << pnode->data->flag << strFieldDelimiter
                << pnode->data->matchkey << strFieldDelimiter << ""
                << strFieldDelimiter << strfields << endl;
          else
            writefile << strfields << endl;
        }
      strfields = "";
      pnode = pnode->next;
    }
  pqc = pqc->next;
}
DedupeLog::GetPtr()->WriteLog("leave WriteOutputfile\n");
}
void
RecordsHash::WriteMatchesFile(bool readall, bool fields, string& matchesfile)
{
DedupeLog::GetPtr()->WriteLog("enter WriteMatchesFile\n");
if (m_pPq == NULL
) return;
qc_t *pqc = m_pPq;
qnode_t *pnode;
vector<int>::iterator indexIter;

 //fprintf(stdout, "the size of outputfiledmap is %d\n", outputfiledmap.size());
string strfields;

while (pqc != NULL)
{

  pnode = pqc->head;
  while (pnode != NULL)
    {
      if (pnode->data->flag == "K" || (readall && pnode->data->flag == "D"))
        {
          //fprintf(stdout, "there's data for keep and drop\n");
          //fprintf(stdout, "size of outputfiledmap:%d\n", outputfiledmap.size());
          vector<int> outputfiledmap =
              pnode->data->datafile->GetOutputfiledmap();
          for (indexIter = outputfiledmap.begin();
              indexIter != outputfiledmap.end(); indexIter++)
                {
            strfields += ",";
            strfields += pnode->data->vdata[*indexIter];
            //fprintf(stdout, "strfields is %s\n", strfields.c_str());
          }
        if (strfields != "")
          strfields = strfields.substr(1);

        if (fields)
          {
            matchesfile += pnode->data->datafile->GetFilename();
            matchesfile += ",";
            matchesfile += pnode->data->flag;
            matchesfile += ",";
            matchesfile += pnode->data->matchkey;
            matchesfile += ",";
            matchesfile += "";
            matchesfile += ",";
            matchesfile += strfields;
            matchesfile += "\n";
          }
        else
          {
            matchesfile += strfields;
            matchesfile += "\n";
          }
      }
    strfields = "";
    pnode = pnode->next;
  }
pqc = pqc->next;
}
DedupeLog::GetPtr()->WriteLog("enter WriteOutputfile\n");
}
void
RecordsHash::WriteDropstofile(string &dropfile)
{
DedupeLog::GetPtr()->WriteLog("enter WriteDropstofile\n");
if (m_pPq == NULL
) return;
qc_t *pqc = m_pPq;
qnode_t *pnode, *p;
vector<int>::iterator indexIter;
string strfields;
while (pqc != NULL)
{
pnode = pqc->head;
p = pnode;
while (p != NULL)
  {
    if (p->data->flag == "D")
      {
        vector<int> outputfiledmap = p->data->datafile->GetOutputfiledmap();
        for (indexIter = outputfiledmap.begin();
            indexIter != outputfiledmap.end(); indexIter++)
              {
          strfields += ",";
          strfields += p->data->vdata[*indexIter];
        }
      dropfile += p->data->datafile->GetFilename();
      dropfile += ",";
      dropfile += pnode->data->flag;
      dropfile += ",";
      dropfile += pnode->data->matchkey;
      dropfile += ",";
      dropfile += "";
      dropfile += ",";
      dropfile += strfields;
      //dropfile += ",";
      //dropfile += p->data->matchkey;
      dropfile += "\n";

      strfields = "";
    }
  p = p->next;
}
pqc = pqc->next;
}
DedupeLog::GetPtr()->WriteLog("leave WriteDropstofile\n");
}
// need provided by invoke
bool
RecordsHash::CmpFunc(FileRecords *pqe1, FileRecords *pqe2)
{
//fprintf(stdout, "matchkey of pqe is %s, matchkey of pqe2 is %s\n",
//pqe1->matchkey.c_str(), pqe2->matchkey.c_str());
return m_pDeduper->GetMatcher()->Compare(pqe1->matchkey, pqe2->matchkey,
pqe1->mtdata, pqe2->mtdata);
}
 //need provided by invoke
/*int RecordsHash::hashKey(FileRecords *pRecord){
 int h = 0, off = 0;
 int len = pRecord->matchkey.length();
 for (int i = 0; i < len; i++)
 h = 31 * h + pRecord->matchkey[off++];
 return h;
 }*/
