/*
 * ParseArgs.h
 *
 *  Created on: 2013-5-27
 *      Author: min
 */

#ifndef PARSEARGS_H_
#define PARSEARGS_H_

#include"getopt.h"
#include <stdio.h>
#include <stdlib.h>

struct CmdArgs_t {
        const char *configFileName;        /* -c option */
        const char *specName;
        const char *outputFileName;     /* -o option */
        bool debug;
        bool input;
        bool fields;
        bool all;
        bool quiet;
        bool summary;
        bool matches;
        bool matrix;
        bool rejects;
        const char *summaryFileName;        /* -s option */
        const char *matchesFileName;        /* -m option */
        const char *matrixFileName;        /* -x option */
        const char *rejectsFileName;        /* -r option */

};

static const struct option longopts[] = {
    { "config",required_argument,NULL, 'c'},
    { "name",optional_argument,NULL, 'n'},
    { "output",required_argument,NULL, 'o'},
    { "debug",no_argument,NULL, 'd'},
    { "input",no_argument,NULL, 'i'},
    { "fields",no_argument,NULL, 'f'},
    { "all",no_argument,NULL, 'a'},
    { "summary",optional_argument,NULL, 's'},
    { "matches",optional_argument,NULL, 'm'},
    { "matrix",optional_argument,NULL, 'x'},
    { "rejects",optional_argument,NULL, 'r'},
    { "quiet",no_argument,NULL, 'q'},
    { "help", no_argument, NULL, 'h' },
    { "?", no_argument, NULL, '?' },
    { NULL, no_argument, NULL, 0 }
};

class ParseArgs
{
public:
  ParseArgs();
  ~ParseArgs();

  bool ProcessCmdline(int argc,char* argv[]);
  void  PrintGlobalArgs(FILE* stream );
  CmdArgs_t *m_pArgs;

protected:
  void DisplayUsage( FILE* stream );

  const char* m_pProgramName;
};

#endif /* PARSEARGS_H_ */
