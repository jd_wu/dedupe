#include<string>
#include<map>
#include<vector>
#include<algorithm>

using namespace std;

struct ConfigCommon{
    const char* postcodetolerance;
    const char* outputformat;
    const char* outputrecorddelimiter;
    const char* outputfielddelimiter;
    const char* outputquotedelimiter;
    const char* outputrecordlength;
    const char* outputfieldlengths;
    const char* outputheaderrecord;
    const char* outputheader;

};

class Deduper;
// specified sort
struct CMapNoSort{
    bool operator() (const string& str1, const string& str2) const{
        return true;
    }
  };
typedef map<string, vector<int>, CMapNoSort> MapNoSort;

//data file base class
class DataFile{
	public:
		DataFile(Deduper *deduper, const string filename, const string filetype);
		virtual ~DataFile(){}

		virtual int ReadAll(){return 0;}
		int SetFieldnames(const string &fieldnames, string delimiter = ",");
		//FileData *m_pfileData;
		void AddAll(const string &data);
		void SetHeaderrecord(const string &headerrecord){m_Headerrecord = headerrecord;}
		void SetHead(const string &head){m_Header = head;}
		string GetHead(){return m_Header;}
		string GetFilename(){return m_Filename;}
		//string GetFilepath(){return m_Filepath;}
		void SetFileorder(const string &fileorder){m_Fileorder = atoi(fileorder.c_str());}
		string GetFiletype(){return m_Filetype;}
		MapNoSort & GetFiledmap(){return m_Fieldmap;}
		vector<string>& GetFieldnames(){return m_Fieldnames;}
		vector<int> & GetOutputfiledmap(){return m_Outputfieldmap;}
		void SetDropcount(){m_Dropcount += 1;}
		int GetDropcount(){return m_Dropcount;}
		int GetTotalcount(){return m_Count;}
		void SetRetaincount(int i = 1){m_Retaincount += i;}
		int GetRetaincount(){return m_Retaincount;}
		void SetOutputfieldmap(const vector<string> &header);
		string GetHeaderrecord(){return m_Headerrecord;}
		void SetMatrixDropcount(string filename);
		void SetTotalMatrixDropcount(string filename, int totalnumber);
		map<string, int>& GetMatrixDropcount(){return m_mapofMatrixDropcount; }
                Deduper *m_pDeduper;
	protected:

                string m_Filename;
                string m_Headerrecord;
                string m_Header;
		//vector<string> m_header;
	private:
                int m_Count;
		string m_Filetype;
		vector<string> m_Fieldnames;
		MapNoSort m_Fieldmap;
		vector<int> m_Outputfieldmap;
		int m_Retaincount;
		int m_Dropcount;
		int m_Fileorder;
		map<string, int> m_mapofMatrixDropcount;




};

class DataFileDelimited: public DataFile{
	public:
		DataFileDelimited(Deduper *deduper, const string filename, const string filetype, const string recorddelimiter, const string fielddelimiter, const string quotedelimiter);
		virtual ~DataFileDelimited(){}

		virtual int ReadAll();
		string GetRecorddelimiter(){return m_Recorddelimiter;}
		string GetFielddelimiter(){return m_Fielddelimiter;}
		string GetQuotedelimiter(){return m_Quotedelimiter;}
	private:
		string m_Recorddelimiter;
		string m_Fielddelimiter;
		string m_Quotedelimiter;

};

class DataFileFixedLength:public DataFile{
	public:
		DataFileFixedLength(Deduper *deduper, const string filename, const string filetype,const string recordlength, const string fieldlengths);
		virtual ~DataFileFixedLength(){}

                string GetRecorddelimiter(){return m_Recorddelimiter;}
		int GetRecordlength(){return m_Recordlength;}
		void SetRecordlength(int recordlength){m_Recordlength = recordlength;}
		vector<int> & GetFiledlengths(){return m_Fieldlengths;}
		string& GetFieldLenghtsforString(){return m_strFieldlengths;}
		void SetFieldlengths(const vector<int> &fieldlengths){m_Fieldlengths = fieldlengths;}
		virtual int ReadAll();
	private:
                string m_Recorddelimiter;
		int m_Recordlength;
		vector<int>m_Fieldlengths;
		string m_strFieldlengths;
};
