/*
 * AlgInterface.h
 *
 *  Created on: 2013-6-17
 *      Author: min
 */

#ifndef ALGINTERFACE_H_
#define ALGINTERFACE_H_

#include<string>
struct gropofparams{
    const char *str1;
    const char *str2;
    bool long_tolerance;
  };
union groupofreturns{
  double i;
  char* j;

};

class AlgInterface{
private:

  static AlgInterface *m_instance; //only one instance to create
  AlgInterface(){}
public:

~AlgInterface(){
  if(m_instance != NULL){
      delete m_instance;
      m_instance = NULL;
  }


};
static AlgInterface* GetPtr(){
    if(m_instance == NULL){
        m_instance = new AlgInterface;
    }
    return m_instance;
};
 static groupofreturns algo_jaro_winkler(gropofparams *params);
 static groupofreturns algo_jaro_distance(gropofparams *params);

 static groupofreturns algo_hamming_distance(gropofparams *params);

 static  groupofreturns algo_levenshtein_distance(gropofparams *params);

 static  groupofreturns algo_damerau_levenshtein_distance(gropofparams *params);

 static groupofreturns algo_soundex(gropofparams *params);

 static  groupofreturns algo_metaphone(gropofparams *params);

 static  groupofreturns algo_nysiis(gropofparams *params);

 static  groupofreturns algo_match_rating_codex(gropofparams *params);

 static  groupofreturns algo_simplefilter( gropofparams *params);
 static  groupofreturns algo_hall_filter(gropofparams *params);
 static  groupofreturns algo_hall_distance(gropofparams *params);
 static groupofreturns algo_simpledistance(gropofparams *params);
};

#endif /* ALGINTERFACE_H_ */
