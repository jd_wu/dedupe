#ifndef MATCHEXCAT_H_
#define MATCHEXCAT_H_
#endif /* MATCHEXCAT_H_ */


#include<map>
#include<string>
#include<vector>
#include <algorithm>

//#include <time.h>
using namespace std;

struct gropofparams;
union groupofreturns;

typedef void (*Pfun)(string &);
typedef groupofreturns (*PAlgs)(gropofparams *);


// specified sort
struct CStrMapNoSort{
    bool operator() (const string& str1, const string& str2) const{
        return true;
    }
  };

//map without sort:
typedef map<string, string, CStrMapNoSort> StrMapNoSort;

struct CMapNoSort;
typedef map<string, vector<int>, CMapNoSort> MapNoSort;


struct MetaData;
class MatcheClose;
//class AlgoInterface;

//Base Class
class MatcherExact{

	public:
		MatcherExact(const string fields);
		virtual ~MatcherExact(){}

		int GetFieldMap(MapNoSort &GetFieldMap, const vector<string> &fieldname);
		virtual string GetMatchkey(const MapNoSort &GetFieldMap, const vector<string > &data,MetaData *metadata);
		virtual bool Compare(string value1, string value2, MetaData *meta1, MetaData *meta2);
	private:
		StrMapNoSort m_Fields;
};

class MatcherStrip: public MatcherExact{

	public:

		MatcherStrip(const string fields, const string stripchars);
		virtual ~MatcherStrip(){}

		virtual string GetMatchkey(const MapNoSort &GetFieldMap, const vector<string > &data, MetaData *metadata);
	private:
		string m_Stripchars;

};

class MatcherNameAddress: public MatcherExact{
	public:

		MatcherNameAddress(const string fields, const string postcodetolerance);
		virtual ~MatcherNameAddress(){}

		virtual string GetMatchkey(const MapNoSort &GetFieldMap, const vector<string > &data,MetaData *metadata);
		virtual bool Compare(string value1, string value2, MetaData *meta1, MetaData *meta2);
	protected:
                const vector<string>getnameprefixes();
                void NameFilter(const string &fields, vector<string> &value);
                virtual string NameFilterfunc(const string &fields, MetaData *metadata);
                string Reduce(const string &value, int minlen);
                void SplitName(string &value, MetaData *metadata);

	private:
		static void RemoveDoubleconsonant( string &value);
		static void RemoveVowel(string &value);
		static void RemoveVowelall(string &value);
		static void RemoveStopword(string &value);
		void IterateUntilmin(string &value, int minlen, Pfun func, Pfun funcall = NULL);
		//bool operator <   (const   MetaData *metadata)   const ;
		void DeleteCompanies(vector<string> &value);
		string FindLongestMatch(const string &value, vector<string> &sequence, string stopchar = " ");
		int GetUnitIndex(string &address);
		string AddressFilterfunc(string &addr, MetaData *metadata);
		string PostcodeFilterfunc(const string &postcode, MetaData *metadata);
		bool IsRegionstopword(const string &value);
		string RegionFilterfunc(const vector<string> &region, MetaData *metadata);
		int m_Postcodetolerance;
		vector<string> m_Nameprefixes;
		vector<string> m_Namesuffixes;
		vector<string> m_Maleprefixes;
		vector<string> m_Femaleprefixes;
		vector<string> m_Companies;
		vector<string> m_Unitprefixes;
		vector<string> m_Regionstopwords;
		vector<string> m_Addresspostfixes;
		PAlgs* distancefunc_nameaddress;
		//clock_t m_time;




};
class MatcherClose: public MatcherExact{
	public:
		MatcherClose(const string fields, const string tolerance, const string algorithm);
		virtual ~MatcherClose(){}

		virtual string GetMatchkey(const MapNoSort &fieldmap, const vector<string > &data, MetaData *metadata);
		virtual bool Compare(string value1, string value2, MetaData *meta1, MetaData *meta2);

                static char* SimpleFilter( const char *str1);
                static  char* HallFilter(const char *str1);
                static  int HallDistance(const char *str1, const char *str2);
                static  int SimpleDistance(const char *str1, const char *str2);
        private:
		double m_Tolerance;
		PAlgs* filterfunc;
		PAlgs* distancefunc;
		string m_Algorithm;
};

class MatcherIndividual: public MatcherNameAddress{
	public:
		
		MatcherIndividual(const string fields, const string postcodetolerance):MatcherNameAddress(fields,postcodetolerance ){}
		virtual ~MatcherIndividual(){}

		virtual string NameFilterfunc(const string &fields, MetaData *metadata);
		virtual bool Compare(string value1, string value2, MetaData *meta1, MetaData *meta2);

};

class MatcherIndividualPrefix: public MatcherIndividual{
	public:
		
		MatcherIndividualPrefix(const string fields, const string postcodetolerance):MatcherIndividual(fields,postcodetolerance ){}
		virtual ~MatcherIndividualPrefix(){}

		virtual bool Compare(string value1, string value2, MetaData *meta1, MetaData *meta2);
};



