/*
 * ParseArgs.cpp
 *
 *  Created on: 2013-5-27
 *      Author: min
 */

#include "parseargs.h"
#include "getopt.h"
#include"dedupelog.h"

#include <string.h>

using namespace std;

static const char *optString = "c:n: :o:difas: :m: :x: :r: :qh?";

ParseArgs::ParseArgs()
{
    // TODO Auto-generated constructor stub
    m_pArgs = (CmdArgs_t*)malloc(sizeof(CmdArgs_t));
    memset(m_pArgs,0, sizeof(CmdArgs_t));
}

ParseArgs::~ParseArgs()
{
  // TODO Auto-generated destructor stub
  if (m_pArgs != NULL)
    {
      free(m_pArgs);
      m_pArgs = NULL;
    }
}

  /* Display program usage, and exit.
   */
  void ParseArgs::DisplayUsage( FILE* stream )
  {
          //puts( "doc2html - convert documents to HTML" );
    //fprintf(stream," Usage: %s options[]\n", m_pProgramName);
    fprintf(stream,
        //"-h [--help] help\n"
          "-c [--config] configuration file (required).\n"
          "-n [--name] spec name if running single spec, omit to run all specs in config file.\n"
          "-o [--output] output file (required).\n"
          "-d [--debug] debugging output while running.\n"
          "-i [--input] print input files after interpreting (good for checking fixed field lengths) (requires --debug).\n"
          "-f [--fields] include calculated fields in output (useful for checking match algorithms).\n"
          "-a [--all] output all records, not just retained records (implies --fields).\n"
          "-s [--summary] summary report (default), optionally to FILE.\n"
          "-m [--matches] list matches report, optionally to FILE.\n"
          "-x [--matrix] matrix report, optionally to FILE.\n"
          "-r [--rejects] rejects (drops) report, optionally to FILE.\n"
          "-q [--quiet] no status or reporting (to stdout, if report files are specified they will still be written).\n");
          /* ... */
  }

bool ParseArgs::ProcessCmdline(int argc,char* argv[])
{

        int opt = 0;
        int longIndex = 0;

        m_pProgramName = argv[0];

        /* Process the arguments with getopt_long(), then
         * populate m_pArgs->
         */
        while(1)
          {
        opt = getopt_long( argc, argv, optString, longopts, &longIndex );
        if (opt == EOF)
          {
            //printf ("non-option ARGV-elements: \n");
            break;
          }
        //while( opt != -1 ) {
                switch( opt ) {
                        case 'c':
                          m_pArgs->configFileName = optarg; /* true */
                          break;
                        case 'n':
                          m_pArgs->specName = optarg;
                          break;
                        case 'o':
                          m_pArgs->outputFileName = optarg;
                          break;
                        case 'd':
                          m_pArgs->debug = true;
                          break;
                        case 'i':
                          m_pArgs->input = true;
                            break;
                        case 'f':
                          m_pArgs->fields = true;
                            break;
                        case 'a':
                          m_pArgs->all = true;
                            break;
                        case 's':
                          m_pArgs->summary = true;
                            m_pArgs->summaryFileName = optarg;
                            break;
                        case 'm':
                            m_pArgs->matches = true;
                            m_pArgs->matchesFileName = optarg;
                            break;
                        case 'x':
                            m_pArgs->matrix = true;
                            m_pArgs->matrixFileName = optarg;
                            break;
                        case 'r':
                          m_pArgs->rejects = true;
                           m_pArgs->rejectsFileName = optarg;
                           break;
                        case 'q':
                           m_pArgs->quiet = true;
                           break;

                        case 'h':               /* fall-through is intentional */
                        case '?':
                            DisplayUsage(stdout);
                            break;

                        default:
                            break;
                        }
        }
        if (m_pArgs->configFileName == NULL || m_pArgs->outputFileName == NULL)
         {
           // if (opt == -1)
            {
                   fprintf (stderr,"error: argument -c/--config and -o/--output are required\n");
                   DedupeLog::GetPtr()->WriteLog("error: argument -c/--config and -o/--output are required\r\n");
                   return false;
            }
         }
        return true;

}
//only for test
void ParseArgs::PrintGlobalArgs(FILE* stream )
{
                          //puts( "doc2html - convert documents to HTML" );
    fprintf (stream, "configfilename: %s,specName: %s, outputfilename: %s, debugflag: %d,inputflag: %d \n", m_pArgs->configFileName, m_pArgs->specName, m_pArgs->outputFileName, m_pArgs->debug,m_pArgs->input );
    fprintf (stream, "fieldsflag: %d, allflag: %d \n", m_pArgs->fields, m_pArgs->all);
    fprintf (stream, "summaryflag: %d, summaryfilename: %s \n", m_pArgs->summary, m_pArgs->summaryFileName);
    fprintf (stream, "matchesflag: %d, matchesfilename: %s \n", m_pArgs->matches,  m_pArgs->matchesFileName);
    fprintf (stream, "matrixflag: %d, matrixfilename: %s \n", m_pArgs->matrix, m_pArgs->matrixFileName);
    fprintf (stream, "rejects: %d, rejecgfilename: %s \n", m_pArgs->rejects, m_pArgs->rejectsFileName);
    fprintf (stream, "quietflag: %d \n", m_pArgs->quiet);

}
