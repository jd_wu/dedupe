//#include <stdio.h>
//#include <stdlib.h>

#include <vector>
#include <algorithm>
#include <functional>

#ifdef _WIN32
#include <process.h>
#include <windef.h>
#else
#include <unistd.h>
#endif

#include "parseargs.h"
#include "getopt.h"
#include "deduper.h"
//#include"datafile.h"
#include "matcherexact.h"
#include "hash.h"
#include "utils.h"
#include "dedupelog.h"

#include "VMProtectSDK.h"

//#include <time.h>

using namespace std;
using namespace utils;

#define PRINT_HELPER(state, flag) if (state & flag) fprintf(stdout, "%s\n ", #flag)

int
main(int argc, char* argv[])
{

  //*****************************************************************licensing part**********************************************************//
  //get Hardware ID:
  //VMProtectBegin("CheckRegistration");
  //check if there's a license file exits? // if not, tell the hardware ID and then exit. else, read the file and verify if it's correct.
  /*ifstream readLicenseFile;
  string licensefile = "serial.lic.txt";
  readLicenseFile.open(licensefile.c_str(), ios::in);
  if (!readLicenseFile) //there's no serial file.
    { //file is not exist
      int nSize = VMProtectGetCurrentHWID(NULL, 0);
      char *buf = new char[nSize];
      VMProtectGetCurrentHWID(buf, nSize);

      fprintf(stdout, "HWID: %s\n", buf);
      fprintf(stdout, "Please register!\n");
      delete[] buf;

      return -1;
    }
  else
    {
      //read the file:
      readLicenseFile.clear();
      readLicenseFile.seekg(0, std::ios::end); // go to the end
      int length = readLicenseFile.tellg(); // report location (this is the length)
      readLicenseFile.seekg(0, std::ios::beg); // go back to the beginning
      char *serial = new char[length]; // allocate memory for a buffer of appropriate dimension
      readLicenseFile.read(serial, length); // read the whole file into the buffer

      int res = VMProtectSetSerialNumber(serial); //verify the serial number
      delete[] serial;
      if (res)
        {
          fprintf(stdout, "Serial number is bad, res = ");
          PRINT_HELPER(res, SERIAL_STATE_FLAG_CORRUPTED);
          PRINT_HELPER(res, SERIAL_STATE_FLAG_INVALID);
          PRINT_HELPER(res, SERIAL_STATE_FLAG_BLACKLISTED);
          PRINT_HELPER(res, SERIAL_STATE_FLAG_DATE_EXPIRED);
          PRINT_HELPER(res, SERIAL_STATE_FLAG_RUNNING_TIME_OVER);
          PRINT_HELPER(res, SERIAL_STATE_FLAG_BAD_HWID);
          PRINT_HELPER(res, SERIAL_STATE_FLAG_MAX_BUILD_EXPIRED);

          fprintf(stdout, "Please register!\n");
          readLicenseFile.close();

          return -1;
        }

      fprintf(stdout, "Serial number is correct\n");
      fprintf(stdout, "You are registered.\n");

    }
  readLicenseFile.close();*/
  //VMProtectEnd();
  //*************************************************************end of licensing part**********************************************************//
  //get process id:
  char processid[20] =
    { 0 };
#ifdef _WIN32
  sprintf(processid, "%d", _getpid());
#else
  sprintf(processid, "%d", getpid());
#endif

  string logfilename = "Dedupe_log_";
  logfilename += processid;
  logfilename += ".log";

  //create log instance:
  DedupeLog *m_DedupeLog = DedupeLog::GetPtr(logfilename);
  m_DedupeLog->WriteLog("Begin the logging");

  //parse the cmd lines, read and parse all of the parameters
  ParseArgs m_Parseargs = ParseArgs();
  if (!m_Parseargs.ProcessCmdline(argc, argv))
    {
      return -1;
    }

  //open the configue file:
  ifstream readFile;
  string strconfigname = m_Parseargs.m_pArgs->configFileName;
  if (strconfigname.rfind(".") == string::npos)
    {
      strconfigname += ".cfg";
    }
  readFile.open(strconfigname.c_str(), ios::in);
  if (!readFile.is_open())
    {
      readFile.close();
      fprintf(stderr, "error: cannot open the file specified %s\n",
          strconfigname.c_str());

      //write to file
      string msg = "error: cannot open the file specified " + strconfigname
          + "\r\n";
      m_DedupeLog->WriteLog(msg);
      return -1;
    }

  string suffix;

  vector<string> vSepcs;
  if (m_Parseargs.m_pArgs->specName != NULL)
    { // allow to specify the name of a single [spec] to run
      vSepcs.push_back(m_Parseargs.m_pArgs->specName);
    }
  else
    { // there's no specified spec name
      string strSection, strline;
      //size_t posofspecstart, posofspec;
      //put all available sections into vector
      while (getline(readFile, strline))
        {
          if (strline.find_first_of('#', 0) == 0)
            continue;
          if (strline.find("[spec") == 0)
            {
              strSection =  strline.substr(strline.find("[")+1, strline.find("]")-1);

              string msg = "Section is " + strSection + "\r\n";
              m_DedupeLog->WriteLog(msg);

              vSepcs.push_back(strSection);
            }
        }
    }
  if (vSepcs.size() == 0)
    {
      fprintf(
          stderr,
          "error: There is no valid specification configured from the configuration file: %s.\n",
          strconfigname.c_str());
    }
  else
    {

      sort(vSepcs.begin(), vSepcs.end()); //sort the [spec], eg. spec1, spec2, spec3....

      vector<string>::iterator specsIter;
      string matchtype, match, stripchars, postcodetolerance, tolerance,
          algorithm;
      for (specsIter = vSepcs.begin(); specsIter != vSepcs.end(); specsIter++)
        {

        //add process id:
        suffix = (*specsIter) + "_" + processid;

        Deduper m_Dedupe = Deduper();
        //create hash table
        RecordsHash *phashofData = new RecordsHash(&m_Dedupe);

        //get matchtype from the config file
        matchtype = m_Dedupe.GetConfigOption((*specsIter).c_str(), "matchtype",
            readFile);

        string msg = "match type is " + matchtype + "\r\n";
       // fprintf(stderr, "matchtype is %s, spec name is %s\n", matchtype.c_str(), (*specsIter).c_str());
        m_DedupeLog->WriteLog(msg);

        //get match from the config file: match what
        //fprintf(stderr, "get match\n");
        match = m_Dedupe.GetConfigOption((*specsIter).c_str(), "match",
            readFile);
        if (match == "")
          {
            fprintf(
                stderr,
                "error: Please define the fields you want to match on in %s. \n",
                (*specsIter).c_str());
            string msg =
                "error: Please define the fields you want to match on in "
                    + (*specsIter) + ".\n";
            m_DedupeLog->WriteLog(msg);
            continue;
            //return -1;
          }

        //fprintf(stdout, "match: %s\n", match.c_str());

        //exact match
        if (matchtype == "" || matchtype == "exact")
          {
            m_Dedupe.SetMatcher(new MatcherExact(match));
          }

        // strip match
        else if (matchtype == "strip")
          {
            //removes the specified characters before doing an exact match
            stripchars = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                "stripchars", readFile);
            //fprintf(stdout, "stripchars is %s\n", stripchars.c_str());
            m_Dedupe.SetMatcher(new MatcherStrip(match, stripchars));

            //fprintf(stdout, "stripchars is %s\n", stripchars.c_str());
          }

        //nameaddress match
        else if (matchtype == "nameaddress")
          {
            postcodetolerance = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                "postcodetolerance", readFile);
            m_Dedupe.SetMatcher(
                new MatcherNameAddress(match, postcodetolerance));
          }

        //individual match
        else if (matchtype == "individual")
          {
            postcodetolerance = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                "postcodetolerance", readFile);
            m_Dedupe.SetMatcher(
                new MatcherIndividual(match, postcodetolerance));
          }

        //individualprefix match
        else if (matchtype == "individualprefix")
          {
            postcodetolerance = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                "postcodetolerance", readFile);
            m_Dedupe.SetMatcher(
                new MatcherIndividualPrefix(match, postcodetolerance));
          }

        //close match
        else if (matchtype == "close")
          {
            tolerance = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                "tolerance", readFile);
            //fprintf(stdout, "tolerance is %s\n", tolerance.c_str());
            algorithm = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                "algorithm", readFile);
            m_Dedupe.SetMatcher(new MatcherClose(match, tolerance, algorithm));
          }

        //other unrecognised type:
        else
          {
            fprintf(stderr, "error: Unrecognised match type specified %s\n",
                matchtype.c_str());
            //write the erro to the log
            string msg = "main(): Unrecognised match type specified "
                + matchtype + "\r\n";
            m_DedupeLog->WriteLog(msg);

            continue;

            //return -1;
          }

        //suppressiononly:
        string suppressionOnly = m_Dedupe.GetConfigOption((*specsIter).c_str(),
            "suppressiononly", readFile);
        msg = "suppressionOnly is" + suppressionOnly + "\r\n";
        m_DedupeLog->WriteLog(msg);
        //fprintf(stdout, "suppressionOnly is %s\n", suppressionOnly.c_str());
        tolowerstr(suppressionOnly);
        m_Dedupe.SetSuppressiononlyFlag(suppressionOnly == "true");
        //fprintf(stdout, "suppresiononly is %s\n", suppressionOnly.c_str());

        //handle prefixes: like file1name, file2name
        string strSection = "[";
        strSection += (*specsIter);
        strSection += "]";
        readFile.clear();
        readFile.seekg(0, ios::beg);

        string strPrefix, line;
        vector<string> vPrefixes; // vector for filenames
        while (!readFile.eof())
          {
            getline(readFile, line);
            utils::trimAndreduceWhiteSpace(line);
            if (line.find(strSection) == 0)
              {
                while (getline(readFile, line) && line.find("[") != 0)
                  {
                    utils::trimAndreduceWhiteSpace(line);
                    if (line.find("file") == 0
                        && line.substr(line.find("=") - 4, 4) == "name")
                      {
                        strPrefix = line.substr(0, line.find("=") - 4);
                        //fprintf(stdout, "prefix = %s\n", strPrefix.c_str());
                        vPrefixes.push_back(strPrefix);
                      }

                  }
              }
          }
        sort(vPrefixes.begin(), vPrefixes.end());

        //handle each file
        string format, filename, filetype, recorddelimiter, fielddelimiter, headerdelimiter,
            quotedelimiter, recordlength, fieldlengths;
        vector<string>::iterator prefixIter;

        //for each file, create a file object which can be delimited or fixedlength
        for (prefixIter = vPrefixes.begin(); prefixIter != vPrefixes.end();
            prefixIter++)
              {
          const char *prefix = (*prefixIter).c_str();
          format = m_Dedupe.GetConfigOption((*specsIter).c_str(), "format",
              readFile, prefix);
          //fprintf(stdout, "format:%s\n",format.c_str());

          if (format == "delimited")
            {
              string msg = "try to create delimited object\n";
              m_DedupeLog->WriteLog(msg);

              //create DataFileDelimited object
              filename = m_Dedupe.GetConfigOption((*specsIter).c_str(), "name",
                  readFile, prefix);

              //fprintf(stdout, "filename is %s\n", filename.c_str());
#ifdef _WIN32
              //fprintf(stdout, "enter win32 settings\n");
              if (filename.find("/") != string::npos)
                {
                  filename.replace(filename.find("/"), 1, "\\");

                }
#else
              if(filename.find("\\") != string::npos)
                {
                  filename.replace(filename.find("\\"), 1, "//");
                }
#endif
              filetype = m_Dedupe.GetConfigOption((*specsIter).c_str(), "type",
                  readFile, prefix);
              //fprintf(stdout, "filetype is %s\n", filetype.c_str());
              /*if(suppressionOnly == "true")
               {
               filetype = "suppression";
               }*/

              recorddelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                  "recorddelimiter", readFile, prefix);
              fielddelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                  "fielddelimiter", readFile, prefix);
              quotedelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                  "quotedelimiter", readFile, prefix);
              headerdelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                  "headerdelimiter", readFile, prefix);
              // fprintf(stderr, "headerdelimiter is %s\n", headerdelimiter.c_str());
              //handle exception

              if (quotedelimiter.length() > 1)
                {
                  //get the first letter
                  quotedelimiter = quotedelimiter[0];
                  // fprintf(stderr, "quotes is %s\n", quotedelimiter.c_str());
                  //write the erro to the log
                  string msg = "There are too many quotes " + quotedelimiter
                      + ", only one is required  and the first one is picked"
                      + "\r\n";
                  m_DedupeLog->WriteLog(msg);

                }
              if (fielddelimiter.length() > 1)
                {
                  if (fielddelimiter.find_first_of(",") == 0)
                    fielddelimiter = fielddelimiter[0];
                  //fprintf(stderr, "fielddelimiters is %s\n", fielddelimiter.c_str());
                  //write the erro to the log
                  string msg = "There are too many fielddelimiters "
                      + fielddelimiter
                      + ", only one is required and the first one is picked"
                      + "\r\n";
                  m_DedupeLog->WriteLog(msg);

                }
              if (recorddelimiter.length() > 1)
                {
                  //get the first letter
                  if (recorddelimiter.find_first_of(",") == 0)
                    recorddelimiter = recorddelimiter[0];
                  //fprintf(stderr, "recorddelimiter is %s\n", recorddelimiter.c_str());
                  //write the erro to the log
                  string msg = "There are too many quotes " + recorddelimiter
                      + ", only one is required  and the first one is picked"
                      + "\r\n";
                  m_DedupeLog->WriteLog(msg);

                }
              //fprintf(stdout, "fielddelimiter is %s\n", fielddelimiter.c_str());

              //utils::unestrcape(recorddelimiter);
              //utils::unestrcape(fielddelimiter);
              //utils::unestrcape(quotedelimiter);

              // fprintf(stderr, "name is %s, fielddelimiter is %s\n", filename.c_str(), fielddelimiter.c_str());
              //system("pause");

              //DataFile *datafile = new DataFileDelimited(&m_Dedupe, filename, filetype, recorddelimiter, fielddelimiter, quotedelimiter);
              //append: there's a datafile list which contains all of the file objects
              m_Dedupe.m_ListofDataFile.push_back(
                  new DataFileDelimited(&m_Dedupe, filename, filetype,
                      recorddelimiter, fielddelimiter, quotedelimiter));
            }
          else if (format == "fixedlength")
            {
              //create DataFileFixedLength object
              filename = m_Dedupe.GetConfigOption((*specsIter).c_str(), "name",
                  readFile, prefix);
#ifdef _WIN32
              if (filename.find("/") != string::npos)
                {
                  filename.replace(filename.find("/"), 1, "\\");
                }
#else
              if(filename.find("\\") != string::npos)
                {
                  filename.replace(filename.find("\\"), 1, "//");
                }
#endif
              filetype = m_Dedupe.GetConfigOption((*specsIter).c_str(), "type",
                  readFile, prefix);
              /*if(suppressionOnly == "true")
               {
               filetype = "suppression";
               }*/
              recordlength = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                  "recordlength", readFile, prefix);
              fieldlengths = m_Dedupe.GetConfigOption((*specsIter).c_str(),
                  "fieldlengths", readFile, prefix);

              //fprintf(stdout, "recordlength is %s, fieldlen is %s\n", recordlength.c_str(), fieldlengths.c_str());
              //DataFile *datafile = new DataFileFixedLength(&m_Dedupe, filename, "suppression", recordlength, fieldlengths);
              //append:there's a datafile list which contains all of the file objects
              m_Dedupe.m_ListofDataFile.push_back(
                  new DataFileFixedLength(&m_Dedupe, filename, filetype,
                      recordlength, fieldlengths));
            }
          else
            continue;
          //other data:

          //set the headerrecord, head and Fileorder for each file object:
          DataFile *datafileTmp = (DataFile *) m_Dedupe.m_ListofDataFile.back();
          if (datafileTmp != NULL)
            {
              datafileTmp->SetHeaderrecord(
                  m_Dedupe.GetConfigOption((*specsIter).c_str(), "headerrecord",
                      readFile, prefix));
              datafileTmp->SetHead(
                  m_Dedupe.GetConfigOption((*specsIter).c_str(), "header",
                      readFile, prefix));
              datafileTmp->SetFileorder(
                  m_Dedupe.GetConfigOption((*specsIter).c_str(), "fileorder",
                      readFile, prefix));

            }

        }

      size_t sizeoflist = m_Dedupe.m_ListofDataFile.size();
      m_Dedupe.m_Dropmatrix = new string[sizeoflist * sizeoflist];

      //TODO
      //initpriority

      //READ INPUT, SORT, GROUP & FLAG

      //create hash table
      // RecordsHash *phashofData  = new RecordsHash(&m_Dedupe);
m_Dedupe.SetHashofData(phashofData);

//priority
        string priority = m_Dedupe.GetConfigOption((*specsIter).c_str(),
            "priority", readFile);
        msg = "priority is" + suppressionOnly + "\r\n";
        m_DedupeLog->WriteLog(msg);
//fprintf(stdout, "priority is %s\n", priority.c_str());
//tolowerstr(suppressionOnly);
        m_Dedupe.SetPriority(priority);
//fprintf(stdout, "suppresiononly is %s\n", suppressionOnly.c_str());

//clock_t  nTimeStart = clock();
        size_t i = 0;
        list<DataFile*>::iterator datafileIter;
        for (datafileIter = m_Dedupe.m_ListofDataFile.begin();
            datafileIter != m_Dedupe.m_ListofDataFile.end(); datafileIter++)
              {
          //input:边读边生成matchkey, 然后放到hash里面，完成插入后就已经分类了
          if ((*datafileIter)->ReadAll() < 0)
            {
              i++;
              datafileIter = m_Dedupe.m_ListofDataFile.erase(datafileIter); //update data file list
              continue;
            }
          //return -1;
        }
      //clock_t  nTimeEnd = clock();
      //fprintf(stdout, "time to read files is %f\n", (double)(nTimeEnd - nTimeStart)/1000);
      // fprintf(stdout, "time to insert to hash is %f\n", (double)phashofData->m_time);

      if (i == sizeoflist) //no file to read
        {
          continue;
        }//
      //readFile.close();
      //flag group
      //nTimeStart = clock();
      //fprintf(stdout, "before FlagGroup\n");
      m_Dedupe.FlagGroup();
      //fprintf(stdout, "after FlagGroup\n");
      //order in flag:
      //['K', 'D', 'S', 'RS', 'RP']
      //nTimeEnd = clock();
      //fprintf(stdout, "time to flagGroup is %f\n", (double)(nTimeEnd - nTimeStart )/1000);
      //outputfile object is created:

      //nTimeStart = clock();
      string fileformat = m_Dedupe.GetConfigOption((*specsIter).c_str(),
          "outputformat", readFile);
      DataFile *outputfile = NULL;
      string outputfilename, outputrecorddelimiter, outputfielddelimiter,
          outputquotedelimiter, outputrecordlength, outputfieldlengths;
      if (fileformat == "delimited")
        {
          //create DataFileDelimited object

          outputfilename = m_Dedupe.filenameize(
              m_Parseargs.m_pArgs->outputFileName, suffix);

          outputrecorddelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
              "outputrecorddelimiter", readFile);
          outputfielddelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
              "outputfielddelimiter", readFile);
          //fprintf(stdout, "outputfielddelimiter is %s\n", outputfielddelimiter.c_str());
          outputquotedelimiter = m_Dedupe.GetConfigOption((*specsIter).c_str(),
              "outputquotedelimiter", readFile);

          if (outputrecorddelimiter.length() > 1)
            {
              if (outputfielddelimiter.find_first_of(",") == 0)
                outputrecorddelimiter = outputrecorddelimiter[0];
            }
          if (outputfielddelimiter.length() > 1)
            {
              if (outputfielddelimiter.find_first_of(",") == 0)
                outputfielddelimiter = outputfielddelimiter[0];
            }
          if (outputquotedelimiter.length() > 1)
            {
              outputquotedelimiter = outputquotedelimiter[0];
            }

          outputfile = new DataFileDelimited(&m_Dedupe, outputfilename,
              "output", outputrecorddelimiter, outputfielddelimiter,
              outputquotedelimiter);
        }
      else if (fileformat == "fixedlength")
        {
          outputfilename = m_Dedupe.filenameize(
              m_Parseargs.m_pArgs->outputFileName, suffix, ".dat");

          outputrecordlength = m_Dedupe.GetConfigOption((*specsIter).c_str(),
              "outputrecordlength", readFile);
          outputfieldlengths = m_Dedupe.GetConfigOption((*specsIter).c_str(),
              "outputfieldlengths", readFile);
          //DataFile *datafile = new DataFileFixedLength(&m_Dedupe, filename, "suppression", recordlength, fieldlengths);
          //append:
          outputfile = new DataFileFixedLength(&m_Dedupe, outputfilename,
              "output", outputrecordlength, outputfieldlengths);
        }
      else
        {
          //TODO: error handling
          fprintf(stderr, "main(): Unrecognised file format specified %s\n",
              fileformat.c_str());
          msg = "main(): Unrecognised file format specified " + fileformat
              + "\n";
          m_DedupeLog->WriteLog(msg);
          continue;
          //write the erro to the log
          //DedupeLog::GetPtr()<<"main(): Unrecognised file format specified %s\n"<<fileformat;
        }
      if (outputfile != NULL
        )
        m_Dedupe.m_OutputFile = outputfile;
      //set header
      string strheader = m_Dedupe.GetConfigOption((*specsIter).c_str(),
          "outputheader", readFile);
      vector<string> vheader;
      if (strheader == "")
        {

          vheader = m_Dedupe.m_Outputfieldnames;
          //fprintf(stdout, "size of vheader is %d\n", vheader.size());
          //join header:
          strheader = m_Dedupe.m_strOutputfilednames;

          //fprintf(stdout, "strheader is %s\n", strheader.c_str());
        }
      else
        utils::splitcsv(vheader, strheader);

      /* vector<string>::const_iterator iter_output = vheader.begin();
       for (; iter_output != vheader.end(); iter_output++){
       fprintf(stderr, "vheader is %s\n", (*iter_output).c_str());
       }*/
      //set output filedmap
      for (datafileIter = m_Dedupe.m_ListofDataFile.begin();
          datafileIter != m_Dedupe.m_ListofDataFile.end(); datafileIter++)
            {
        // if((*datafileIter)->GetFiletype() == "mail")
        (*datafileIter)->SetOutputfieldmap(vheader);
      }

    bool fields = (m_Parseargs.m_pArgs->fields || m_Parseargs.m_pArgs->all);

//fprintf(stdout, "strheader:%s\n", strheader.c_str());
    if (fields)
      {

        string str = "filename";
        str += ",";
        str += "flag";
        str += ",";
        str += "matchkey";
        str += ",";
        str += "groupkey";
        str += ",";
        strheader = str + strheader;
        //fprintf(stdout, "strheader is %s\n", strheader.c_str());
        /*if(fileformat == "fixedlength"){
         DataFileFixedLength *fixedlenfile = dynamic_cast<DataFileFixedLength*>(outputfile);
         if(fixedlenfile != NULL){
         const int strlength[] = {30, 2, 50, 10};
         vector<int> vec(strlength, strlength+sizeof(strlength) / sizeof(int));
         copy(fixedlenfile->GetFiledlengths().begin(), fixedlenfile->GetFiledlengths().end(), back_inserter(vec));
         fixedlenfile->SetFieldlengths(vec);
         fixedlenfile->SetRecordlength(92);
         }
         }*/
      }
//else{
//     strheader = strheader.substr(1);
//   }

    outputfile->SetHead(strheader);
    msg = "strheader is " + strheader + "\n";
    m_DedupeLog->WriteLog(msg);

//write the records
    ofstream writeOutputfile;
//construct the file path
    writeOutputfile.open(outputfilename.c_str(), ios::out);

    if (!writeOutputfile.is_open())
      {
        writeOutputfile.close();
        fprintf(stderr, "error: cannot open the output file specified %s\n",
            outputfilename.c_str());
        string msg = "error: cannot open the output file specified "
            + outputfilename + "\n";
        m_DedupeLog->WriteLog(msg);
      }

    phashofData->WriteOutputfile(m_Parseargs.m_pArgs->all, writeOutputfile,
        fields);
    writeOutputfile.close();

//reports:
//write matches to screen or file
//fprintf(stdout, "start to write match file, strhead is %s\n", strheader.c_str());
    string matchesfile;
    phashofData->WriteMatchesFile(m_Parseargs.m_pArgs->all, fields,
        matchesfile);

    if (m_Parseargs.m_pArgs->matches)
      {
        string matchesfilename = m_Dedupe.filenameize(
            m_Parseargs.m_pArgs->matchesFileName, suffix);

        ofstream writeMatchesfile;
        //construct the file path
        writeMatchesfile.open(matchesfilename.c_str(), ios::out);
        if (!writeMatchesfile.is_open())
          {
            writeMatchesfile.close();
            fprintf(stderr, "error: cannot open the file specified %s\n",
                matchesfilename.c_str());
            string msg = "error: cannot open the file specified "
                + matchesfilename + "\n";
            m_DedupeLog->WriteLog(msg);
          }

        //handle head, replace with comma for those delimiter is not comma
        //if()
        //writeMatchesfile<<"Matches:"<<endl;
        writeMatchesfile << strheader << endl;
        writeMatchesfile << matchesfile;
        writeMatchesfile << endl;
        writeMatchesfile.close();

      }
    else if (!m_Parseargs.m_pArgs->quiet)
      {
        fprintf(stdout, "%s\n%s\n", strheader.c_str(), matchesfile.c_str());
      }

//write summary to screen or file
    if (m_Parseargs.m_pArgs->summary)
      {
        string summaryfilename = m_Dedupe.filenameize(
            m_Parseargs.m_pArgs->summaryFileName, suffix);
        ofstream writeSummaryfile;
        //construct the file path
        writeSummaryfile.open(summaryfilename.c_str(), ios::out);
        if (!writeSummaryfile.is_open())
          {
            writeSummaryfile.close();
            fprintf(stderr, "error: cannot open the file specified %s\n",
                summaryfilename.c_str());
            string msg = "error: cannot open the file specified "
                + summaryfilename + "\n";
            m_DedupeLog->WriteLog(msg);
          }
        //writeSummaryfile<<"Summary:"<<endl;
        writeSummaryfile << "type" << "," << "file" << "," << "totalcount"
            << "," << "retaincount" << "," << "dropcount" << endl;
        for (datafileIter = m_Dedupe.m_ListofDataFile.begin();
            datafileIter != m_Dedupe.m_ListofDataFile.end(); datafileIter++)
              {
          writeSummaryfile << (*datafileIter)->GetFiletype() << ","
              << (*datafileIter)->GetFilename() << ","
              << (*datafileIter)->GetTotalcount() << ","
              << (*datafileIter)->GetRetaincount() << ","
              << (*datafileIter)->GetDropcount() << endl;
        }
      writeSummaryfile << endl;
      writeSummaryfile.close();
    }
  else if (!m_Parseargs.m_pArgs->quiet)
    { //print to screen
      fprintf(stdout, "type,file,totalcount,retaincount,dropcount\n");
      for (datafileIter = m_Dedupe.m_ListofDataFile.begin();
          datafileIter != m_Dedupe.m_ListofDataFile.end(); datafileIter++)
            {
        fprintf(stdout, "%s,%s,%d,%d,%d\n",
            (*datafileIter)->GetFiletype().c_str(),
            (*datafileIter)->GetFilename().c_str(),
            (*datafileIter)->GetTotalcount(), (*datafileIter)->GetRetaincount(),
            (*datafileIter)->GetDropcount());
      }
  }

//write Matrix to screen or file:
string MatrixHeader1 = "type";
MatrixHeader1 += ",,";

string MatrixHeader2 = ",file,";
MatrixHeader2 += "count";
//writeMatrixfile<<<<"totalcount"<<","<<"retaincount"<<","<<"dropcount"<<endl;
for (datafileIter = m_Dedupe.m_ListofDataFile.begin();
    datafileIter != m_Dedupe.m_ListofDataFile.end(); datafileIter++)
      {
  MatrixHeader1 += ",";
  MatrixHeader1 += (*datafileIter)->GetFiletype();

  MatrixHeader2 += ",";
  MatrixHeader2 += (*datafileIter)->GetFilename();

  //writeMatrixfile<<(*datafileIter).getFiletype()<<","<<(*datafileIter).getfilename()<<","<<(*datafileIter).gettotalcount()<<","<<(*datafileIter).getretaincount()<<","<<(*datafileIter).getdropcount()<<endl;
}
MatrixHeader1 += ",";
MatrixHeader1 += "\n";

MatrixHeader2 += ",dropcount";
MatrixHeader2 += "\n";

 //write data
 //writeMatrixfile<<"type"<<","<<"file"<<","<<"totalcount"<<","<<"retaincount"<<","<<"dropcount"<<endl;
string strData, strFilename;
list<DataFile*>::iterator iter;
char *buf = new char[100];
memset(buf, 0, 100 * sizeof(char));
map<string, int> mapofMatrix;
 //fprintf(stdout, "before write matrix\n");
for (datafileIter = m_Dedupe.m_ListofDataFile.begin();
  datafileIter != m_Dedupe.m_ListofDataFile.end(); datafileIter++)
    {
strData += (*datafileIter)->GetFiletype();
strData += ",";
strData += (*datafileIter)->GetFilename();
strData += ",";
sprintf(buf, "%d", (*datafileIter)->GetTotalcount());
strData += buf;
//fprintf(stdout, "strData is %s\n", strData.c_str());
memset(buf, 0, 100 * sizeof(char));
mapofMatrix = (*datafileIter)->GetMatrixDropcount();
//fprintf(stdout, "count of m_ListofDataFile is %d\n", m_Dedupe.m_ListofDataFile.size());

for (iter = m_Dedupe.m_ListofDataFile.begin();
    iter != m_Dedupe.m_ListofDataFile.end(); iter++)
      {
  strFilename = (*iter)->GetFilename();
  //fprintf(stdout, "Filename is %s\n", strFilename.c_str());
  if (mapofMatrix.find(strFilename) != mapofMatrix.end())
    {
      strData += ",";
      //fprintf(stdout, "strData is %s, dropcount is %d\n", strData.c_str(), mapofIter->second);
      sprintf(buf, "%d", mapofMatrix[strFilename]);
      strData += buf;
      memset(buf, 0, 100 * sizeof(char));
    }
  else
    {
      strData += ",";
      strData += "0";
    }
}
strData += ",";
sprintf(buf, "%d", (*datafileIter)->GetDropcount());
strData += buf;
 //fprintf(stdout, "the end of strData is %s\n", strData.c_str());
strData += "\n";
 //writeMatrixfile<<strData;
}

 //writeMatrixfile<<endl;
delete[] buf;

if (m_Parseargs.m_pArgs->matrix)
{
string matrixfilename = m_Dedupe.filenameize(
  m_Parseargs.m_pArgs->matrixFileName, suffix);
ofstream writeMatrixfile;
 //construct the file path
writeMatrixfile.open(matrixfilename.c_str(), ios::out);
if (!writeMatrixfile.is_open())
{
  writeMatrixfile.close();
  fprintf(stderr, "error: cannot open the file specified %s\n",
      matrixfilename.c_str());
  msg = "error: cannot open the file specified " + matrixfilename + "\n";
  m_DedupeLog->WriteLog(msg);
}
 //writeMatrixfile<<"Matrix:"<<endl;

writeMatrixfile << MatrixHeader1;
writeMatrixfile << MatrixHeader2;

writeMatrixfile << strData;
writeMatrixfile.close();
}
else if (!m_Parseargs.m_pArgs->quiet)
{
fprintf(stdout, "%s%s%s", MatrixHeader1.c_str(), MatrixHeader2.c_str(),
  strData.c_str());
}

 //write dropfiles to screen or file:
string strDropfiles;
phashofData->WriteDropstofile(strDropfiles);

if (m_Parseargs.m_pArgs->rejects)
{
string rejectsfilename = m_Dedupe.filenameize(
  m_Parseargs.m_pArgs->rejectsFileName, suffix);
ofstream writeRejectsfile;
 //construct the file path
writeRejectsfile.open(rejectsfilename.c_str(), ios::out);
if (!writeRejectsfile.is_open())
{
  writeRejectsfile.close();
  fprintf(stderr, "error: cannot open the file specified %s\n",
      rejectsfilename.c_str());
  string msg = "error: cannot open the file specified " + rejectsfilename
      + "\n";
  m_DedupeLog->WriteLog(msg);
}
 //writeRejectsfile<<"Rejects (Drops):"<<endl;
writeRejectsfile /*<< "file" << "," */<< strheader << endl;
writeRejectsfile << strDropfiles;
writeRejectsfile << endl;
writeRejectsfile.close();
}
else if (!m_Parseargs.m_pArgs->quiet)
{
fprintf(stdout, "file, %s\n%s\n", strheader.c_str(), strDropfiles.c_str());
}
 //release resources

//nTimeEnd = clock();
//fprintf(stdout, "time to write 4 fiels is %f\n", (double)(nTimeEnd - nTimeStart )/1000);

delete phashofData;
//phashofData = NULL;

delete outputfile;
//outputfile = NULL;
//fprintf(stdout, "before release resource\n");
}
}
 // fprintf(stdout, "before close file\n");
readFile.close();
 //writeLogFile.close();
//fprintf(stdout, "after close file\n");
if (m_DedupeLog != NULL)
{
delete m_DedupeLog;
//m_DedupeLog = NULL;
}

return 0;
}

