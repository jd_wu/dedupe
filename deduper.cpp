/*
 * Deduper.cpp
 *
 *  Created on: 2013-5-19
 *      Author: min
 */

#include "deduper.h"
#include <fstream>
#include<memory.h>
#include"utils.h"
#include "hash.h"
#include"matcherexact.h"
#include"DedupeLog.h"
using namespace utils;
using namespace std;

typedef map<string, string> mapType;
const mapType::value_type TypeFlag[] =
  { mapType::value_type("mail", "M"), mapType::value_type("suppression", "S"),
      mapType::value_type("retainsubst", "RS"), mapType::value_type(
          "retainplus", "RP"), mapType::value_type("Retain", "R") };
const mapType Deduper::m_MapofTypeFlag(TypeFlag, TypeFlag + 5);

Deduper::Deduper()
{
  // TODO Auto-generated constructor stub
  //self.args = args
  //self.typeflagmap = {'mail': 'M', 'suppression': 'S', 'retainsubst': 'RS', 'retainplus': 'RP', 'output': 'O'}
  m_pMatcher = NULL;
  m_TotalRetainCount = 0;
  m_TotalDropCount = 0;
  m_Suppressiononly = false;
  m_HashofData = NULL;
  m_Priority = "";
  //RecordsHash m_HashofData  = RecordsHash();
  //m_OutputFile = NULL;
  //m_Dropmatrix = NULL;

}

Deduper::~Deduper()
{
  // TODO Auto-generated destructor stub
  //create hashqueue object
  m_Suppressiononly = false;
  std::list<DataFile*>::iterator iter1, iter2, temp;
  for (iter1 = m_ListofDataFile.begin(), iter2 = m_ListofDataFile.end();
      iter1 != iter2;)
        {
    temp = iter1;
    ++iter1;
    delete (*temp);
  }

if (m_pMatcher != NULL)
  {
    delete m_pMatcher;
    m_pMatcher = NULL;
  }
//data = NULL;
//self.outputfieldnames = []
//self.dropmatrix = []
m_TotalRetainCount = 0;
m_TotalDropCount = 0;
}
string
Deduper::GetConfigOption(const char *sectionname, const char *keyname,
  ifstream& readfile, const char *prefix)
{

string reValue;

string strSection = "[";
strSection += sectionname;
strSection += "]";

string strKey;
if (prefix != NULL)
  {
    strKey = prefix;
    strKey += keyname;
  }
else
  {
    strKey = keyname;
  }
//strKey += "=";
if ((reValue = GetOption(strSection, strKey, readfile)) == "")
  {
    strKey = keyname;
    //strKey += "=";

    //if there's no value in the config file for the corresponding key, get the common/commonfile value
    if ((reValue = GetOption("[common]", strKey, readfile)) == "")
      reValue = GetOption("[commonfile]", strKey, readfile);
  }
return reValue;

}
string
Deduper::GetOption(const string &strSection, const string &strKey,
  ifstream& readfile)
{

string reValue;

readfile.clear();
readfile.seekg(0, ios::beg);
if (!readfile.is_open())
  {
    readfile.close();
    fprintf(stderr, "error: cannot open the file\n");
    string msg = "error: cannot open the file\n";
    DedupeLog::GetPtr()->WriteLog(msg);
    return reValue;
  }
string line;
while (!readfile.eof())
  {
    getline(readfile, line); //read the line
    //utils::trimAndreduceWhiteSpace(line); //space is allowed for each line
    if (reValue != "")
      {
        if (reValue.find("#") != string::npos) //如果查找到"#" 就去掉包括“#”号后面的字符串
          {
            reValue = reValue.substr(0, reValue.find("#"));
          }
        //utils::trim(reValue);
        if (reValue.find_last_of(",") == (reValue.length() - 1)) //if there's comma at the end of line, it means there are more than 1 line
          {
            reValue += line;
            //fprintf(stderr, "reValue2: %s, strkey is %s\n", reValue.c_str(), strKey.c_str());
            string msg = "There are multiple lines: reValue is " + reValue + ", strKey is " + strKey + "\n";
            DedupeLog::GetPtr()->WriteLog(msg);
          }
        else
          { //trim whitespace

            //fprintf(stderr, "reValue3: %s, strkey is %s\n", reValue.c_str(), strKey.c_str());

            string msg = "reValue is " + reValue + ", strKey is " + strKey + "\n";
            DedupeLog::GetPtr()->WriteLog(msg);
            return reValue;
          }
      }
    else if (line.find(strSection) == 0) //find the section
      {
        //getline(readfile, line); // read the next line
        //utils::trimAndreduceWhiteSpace(line);
        string tmp;
        int i = -1;
        while ( !readfile.eof())
          {
            getline(readfile, line);
            //fprintf(stderr, "cannot find the key\n");
            if (line.find("[") == 0) // out of the specified section
              return reValue;
            if(line.find(strKey) == 0){
                tmp = line.substr(strKey.size());
                utils::trim(tmp);
                if((i = tmp.find("=")) == 0){
                    reValue = tmp.substr(i+ 1);
                    utils::trim(reValue);
                   // fprintf(stdout, "strKey is %s, reValue is %s\n", strKey.c_str(), reValue.c_str());
                    break;
                }
            }


            //utils::trimAndreduceWhiteSpace(line);
          }

       /* if (line.find(strKey) == 0) //find the key
          {
            reValue = line.substr(line.find("=") + 1);
          }*/
      }
  }
return reValue;
}

void
Deduper::SetMatcher(MatcherExact *pMatcher)
{

if (m_pMatcher != NULL)
  {
    delete m_pMatcher;
    m_pMatcher = NULL;
  }
m_pMatcher = pMatcher;
//m_HashofData.SetMatcher(m_pMatcher);
}
void Deduper::SetPriority(string strPriority){
  //fprintf(stdout, "enter SetPriority\n");
  if(strPriority != ""){
      m_Priority = strPriority;
      m_HashofData->SetPriorityCondition(strPriority);
  }
  //fprintf(stdout, "leave SetPriority\n");
}

//read file data
void
Deduper::AddData(DataFile * datafile, const string &data)
{
 //fprintf(stdout, "begin AddData\n");
// if(datafile != NULL){
FileRecords filerecords;

map<string, string>::const_iterator iter = m_MapofTypeFlag.find(
    datafile->GetFiletype());
if (iter != m_MapofTypeFlag.end())
  {
    filerecords.flag = iter->second;
  }

string strData = data;
//utils::trim(strData); //trim space
//fprintf(stdout, "data:%s\n", data.c_str());

vector<string> tmpData;

//put data into vector:
DataFileDelimited* delimitedfile = dynamic_cast<DataFileDelimited*>(datafile);
if (delimitedfile != NULL)
  {
    string strfileddelimiter = delimitedfile->GetFielddelimiter();
    string strquotedelimiter = delimitedfile->GetQuotedelimiter();

    string delimiter = utils::unestrcape(strfileddelimiter);

    //fprintf(stdout, "fileddelimiter is %s, quote is %s, delimiter is %s\n ", strfileddelimiter.c_str(),  strquotedelimiter.c_str(), delimiter.c_str());
    //fprintf(stdout, "read data into vector\n");
    utils::splitcsv(filerecords.vdata, strData, delimiter,
        strquotedelimiter[0]);

    filerecords.datafile = delimitedfile;
    // fprintf(stdout, "datafile assignment\n");

  }
else
  {
    //fprintf(stdout, "enter fixedlength\n");
    DataFileFixedLength *fixedlenfile =
        dynamic_cast<DataFileFixedLength*>(datafile);
    if (fixedlenfile != NULL)
      {
        utils::splitfixedlenfile(filerecords.vdata, strData,
            fixedlenfile->GetFiledlengths(), fixedlenfile->GetRecordlength());
        filerecords.datafile = fixedlenfile;
        tmpData = filerecords.vdata;

      }
    else
      {
        //print error
        fprintf(stderr, "error: adddata(): unrecognised datafile point.\n");
        //write the erro to the log
        string msg = "error: adddata(): unrecognised datafile point.\n";
        DedupeLog::GetPtr()->WriteLog(msg);
      }
  }
//fprintf(stdout, "create MetaDta\n");
MetaData *metaData = new MetaData();
filerecords.mtdata = metaData;

//create matchkey
//fprintf(stdout, "Adddata, before createMatchkey\n");

//handle data: remove quote:

if (delimitedfile != NULL)
  {
    string strquotedelimiter = delimitedfile->GetQuotedelimiter();
    //fprintf(stdout, "quotedelimiter is %s\n", strquotedelimiter.c_str());

    vector<string>::iterator iter = filerecords.vdata.begin();
    string strData;
    for (; iter != filerecords.vdata.end(); iter++)
      {
      strData = *iter;
     // fprintf(stdout, "data in vector is %s\n", strData.c_str());
      utils::stripquote(strData, strquotedelimiter[0]);
     // fprintf(stdout, "data in vector after stripquote is %s\n", strData.c_str());
      tmpData.push_back(strData);
    }
}

//fprintf(stdout, "before GetMatchkey:%s\n", strData.c_str());
filerecords.matchkey = m_pMatcher->GetMatchkey(datafile->GetFiledmap(), tmpData,
  filerecords.mtdata);
//fprintf(stdout, "GetMatchkey:%s\n", filerecords.matchkey.c_str());
string msg = "The MatchKey is %s: " + filerecords.matchkey + "\n";
DedupeLog::GetPtr()->WriteLog(msg);


//insert data into hash table
if (m_HashofData->HashInsertElement(&filerecords) < 0)
{
  //error
  //print error
  fprintf(stderr, "error: adddata(): fail to insert the hash table.\n");
  //write the erro to the log
  string msg = "error: adddata(): fail to insert the hash table.\n";
  DedupeLog::GetPtr()->WriteLog(msg);
}
//fprintf(stdout, "leave AddData\n");
}
void
Deduper::FlagGroup()
{

if (m_Suppressiononly){
  //fprintf(stdout, "suppression only is on\n");
  DedupeLog::GetPtr()->WriteLog("suppression only is on\n");
  //m_HashofData->SetDropcountforSuppression(); //remove all of records from mail that matches those found in suppression file
  //m_HashofData->SetRetaincountforSuppression();
  m_HashofData->SetFlagForSuppressionOnly();
  //fprintf(stdout, "finish flaggroup\n");
  DedupeLog::GetPtr()->WriteLog("finish flaggroup\n");
}
else{

  //fprintf(stdout, "begin SetRetainAndDropFlag\n");
  DedupeLog::GetPtr()->WriteLog("begin SetRetainAndDropFlag\n");
  m_HashofData->SetRetainAndDropFlag();
  //fprintf(stdout, "finish SetRetainAndDropFlag\n");
  DedupeLog::GetPtr()->WriteLog("finish SetRetainAndDropFlag\n");
  /*
   m_HashofData->SetDropcountforInternaldupes();
   fprintf(stdout, "finish SetDropcountforInternaldupes\n");
   m_HashofData->SetDropcountforSuppression();
   fprintf(stdout, "finish SetDropcountforSuppression\n");
   m_HashofData->SetDropcountforCrossfiles();
   fprintf(stdout, "finish SetDropcountforCrossfiles\n");*/
}
}

string
Deduper::filenameize(string name, string suffix, string ext)
{
string filename = name;
string fileext;
if (name.rfind(".") != string::npos)
{
  filename = name.substr(0, name.find(".") - 1);
  fileext = name.substr(name.rfind(".") + 1);
}
else
filename = name;
if (fileext != ext)
{
  filename = name;
  fileext = ext;
}
if (suffix != "")
{
  suffix = "_" + suffix;
}
return filename + suffix + fileext;
}
