/*
 * AlgInterface.cpp
 *
 *  Created on: 2013-6-17
 *      Author: min
 */

#include "alginterface.h"
#include "matcherexact.h"
extern "C"{
#include"jellyfish.h"
}
/*
AlgInterface::AlgInterface()
{
  // TODO Auto-generated constructor stub

}

AlgInterface::~AlgInterface()
{
  // TODO Auto-generated destructor stub
}
*/
AlgInterface* AlgInterface::m_instance = NULL;
groupofreturns AlgInterface::algo_jaro_winkler(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) jaro_winkler(params->str1, params->str2, params->long_tolerance);
  return returns;
}
groupofreturns AlgInterface::algo_jaro_distance(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) jaro_distance(params->str1, params->str2);
  return returns;
}

groupofreturns AlgInterface::algo_hamming_distance(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) hamming_distance(params->str1, params->str2);
  return returns;
}

groupofreturns AlgInterface::algo_levenshtein_distance(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) levenshtein_distance(params->str1, params->str2);
  return returns;
}

groupofreturns AlgInterface::algo_damerau_levenshtein_distance(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) levenshtein_distance(params->str1, params->str2);
  return returns;
}

groupofreturns AlgInterface::algo_soundex(gropofparams *params){
  groupofreturns returns;
  returns.j = soundex(params->str1);
  return returns;
}

groupofreturns AlgInterface::algo_metaphone(gropofparams *params){
  groupofreturns returns;
  returns.j = metaphone(params->str1);
  return returns;
}

groupofreturns AlgInterface::algo_nysiis(gropofparams *params){
  groupofreturns returns;
  returns.j = nysiis(params->str1);
  return returns;
}

groupofreturns AlgInterface::algo_match_rating_codex(gropofparams *params){
  groupofreturns returns;
  returns.j = match_rating_codex(params->str1);
  return returns;
}

groupofreturns AlgInterface::algo_simplefilter( gropofparams *params){
  groupofreturns returns;
  returns.j = MatcherClose::SimpleFilter(params->str1);
  return returns;
}
groupofreturns AlgInterface::algo_hall_filter(gropofparams *params){
  groupofreturns returns;
  returns.j = MatcherClose::HallFilter(params->str1);
  return returns;
}
groupofreturns AlgInterface::algo_hall_distance(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) MatcherClose::HallDistance(params->str1, params->str2);
  return returns;
}
groupofreturns AlgInterface::algo_simpledistance(gropofparams *params){
  groupofreturns returns;
  returns.i =(double) MatcherClose::SimpleDistance(params->str1, params->str2);
  return returns;
}

